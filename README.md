# Coruja: API para um Reconhecedor de Voz Livre em Português Brasileiro

:warning: Esta documentação foi produzida em agosto de 2010 sobre a versão 4.1.5
do Julius decoder. O projeto do Coruja foi descontinuado e não possui mais
suporte contínuo do Grupo FalaBrasil.

Autores:  Nelson Neto, Carlos Silva, Pedro Batista e Aldebaro Klautau

## A Interface de Programação
Ao promover o amplo desenvolvimento de aplicações baseadas em reconhecimento de 
voz, os autores observaram que não era suficiente apenas tornar os recursos 
disponı́veis, tais como modelos de linguagem. Esses recursos são úteis para 
pesquisadores, mas o que a maioria dos programadores desejam é a praticidade 
oriunda de uma interface de programação de aplicativos (API). Por isso, foi 
necessário complementar o código que faz parte do pacote de distribuição do
[Julius](https://github.com/julius-speech/julius). O Julius é um reconhecedor 
_open-source_ de alta performance para grandes vocabulários.

![Modelo de Interação com a API](./doc/coruja_arch.png)

Assim, uma API foi desenvolvida na linguagem de programação C++ com a
especificação _common language runtime_, que permite comunicação entre as 
linguagens suportadas pela plataforma Microsoft .NET. A API proposta permite o
controle em tempo-real do _engine_ para reconhecimento de voz, Julius, e da 
interface de áudio do sistema. Como pode ser visto na Figura acima, as aplicações 
interagem com o reconhecedor Julius através da API. Basicamente, a API "esconde"
do programador detalhes de baixo nı́vel relacionados à operação do engine.
A API proposta é parte do Projeto FalaBrasil. Esse projeto não restringe
o uso de seus recursos para fins comerciais e não-comerciais. 
A seguir os métodos e eventos da API serão apresentados. 

## Métodos e Eventos
Visto que a API suporta objetos compatı́veis com o modelo de automação
_component object model_ (COM), é possı́vel acessar e manipular (i.e. ajustar
propriedades, invocar métodos) objetos de automação compartilhados que são
exportados por outras aplicações. Do ponto de vista da programação, a API
consiste de uma classe principal denominada SREngine. Essa classe expõe à
aplicação um conjunto de métodos e eventos descritos na Tabela abaixo.

| Métodos/Eventos            | Descrição Básica                                    |
|:--------------------------:|:---------------------------------------------------:|
|<tt>SREngine()</tt>         | Método para carregar e inicializar o reconhecedor   |
|<tt>loadGrammar()</tt>      | Método para carregar gramática SAPI XML             |
|<tt>addGrammar()</tt>       | Método para carregar gramática nativa do Julius     |
|<tt>startRecognition()</tt> | Método para iniciar o reconhecimento                |
|<tt>stopRecognition()</tt>  | Método para pausar/parar o reconhecimento           |
|<tt>OnRecognition</tt>      | Evento chamado quando alguma sentença é reconhecida |
|<tt>OnSpeechReady</tt>      | Evento chamado quando o reconhecimento é ativado    |

A classe SREngine permite que a aplicação controle aspectos do reconhecedor 
Julius. Essa classe possibilita que a aplicação carregue os modelos acústico e
de linguagem a serem utilizados, inicie e pare o reconhecimento e receba eventos
e resultados provenientes do engine de reconhecimento.

Uma aplicação baseada em voz precisa criar, carregar e ativar uma gramática,
que essencialmente indica o método de reconhecimento empregado, ou seja, ditado 
ou livre de contexto. A gramática para ditado é implementada via modelo de
linguagem, que define um extenso conjunto de palavras. Por sua vez, essas
palavras podem ser pronunciadas de uma forma relativamente irrestrita. Já a
gramática livre de contexto age como um modelo de linguagem. Ela provê ao
reconhecedor regras que definem o que pode ser dito.

Através do método loadGrammar é possı́vel carregar uma gramática livre
de contexto especificada no formato Microsoft Speech API (SAPI) XML. Para
tornar isso possı́vel, um conversor gramatical foi desenvolvido e integrado ao
método <tt>loadGrammar()</tt>. Essa ferramenta permite que o sistema converta 
auto maticamente uma gramática de reconhecimento especificada no padrão 
[SAPI Text Grammar Format](https://docs.microsoft.com/en-us/previous-versions/windows/desktop/ms723635(v%3Dvs.85)) 
para o formato suportado pelo Julius. O procedimento
de conversão usa as regras gramaticais SAPI para encontrar as conexões
permitidas entre as palavras, usando o nome das categorias como nós terminais.
Isso também define as palavras candidatas em cada categoria, juntamente com
as suas respectivas pronúncias.

É importante salientar que o conversor ainda não suporta regras gramaticais
recursivas, facilidade suportada pelo Julius. Para esse tipo de funcionalidade
deve-se carregar a gramática nativa do Julius através do método 
<tt>addGrammar()</tt>.
A especificação para esse tipo de gramática pode ser encontrada 
[aqui](https://github.com/julius-speech/grammar-kit).

O método startRecognition, responsável por iniciar o processo de reconhecimento, 
basicamente ativa as regras gramaticais e abre o stream de áudio. Similarmente, 
o método stopRecognition desativa as regras e fecha o stream de áudio.

Adicionalmente aos métodos, alguns eventos também foram implementados.
O evento OnSpeechReady sinaliza que o engine está ativado para reconhecimento. 
Em outras palavras, ele surge toda vez que o método startRecognition
é invocado. Já o evento OnRecognition acontece sempre que o resultado do 
reconhecimento encontra-se disponı́vel, juntamente com o seu nı́vel de confiança.

A medida de confiança do que foi reconhecido pelo engine é essencial para
aplicações reais, dado que sempre ocorrerá erros de reconhecimento e, portanto,
os resultados podem ser aceitos ou rejeitados. A seqüencia de palavras 
reconhecidas e o seu nı́vel de confiança são passados da API para a aplicação 
através da classe RecoResult.

A seguir será comprovado que, fazendo uso do conjunto limitado de métodos
e eventos apresentados acima, é viável construir compactas aplicações baseadas
em voz com a API proposta.

## PPT Controller
Para ilustrar a funcionalidade e a possibilidade de comunicação da API com
outros programas, será apresentado o PPTController, ilustrado na Figura abaixo.

O PPTController é uma aplicação escrita na linguagem de programação C#
que faz uso de recursos próprios construı́dos no Laboratório de Processamento de
Sinais da Universidade Federal do Pará. Com esse aplicativo é possı́vel controlar
uma apresentação de slides do Microsoft Office PowerPoint 2007 via comandos
de voz. O usuário controla o documento através de comandos especı́ficos:

- **Mostrar**: primeiro comando que deve ser enviado, pois abre o slide show.
- **Próximo** ou **Avançar**: ir para o próximo slide da apresentação.
- **Anterior** ou **Voltar**: voltar para o slide anterior na apresentação.
- **Primeiro**: ir imediatamente para o primeiro slide da apresentação.
- **Último**: ir imediatamente para o último slide da apresentação.
- **Fechar**: fechar a apresentação e voltar para a tela principal do aplicativo.

![Tela principal do software PPTController](./doc/ppt_controller.png)

O pacote de distribuição da API contém quatro DLL’s: julius.dll, sent.dll,
mkfa.dll e LapsAPI.dll. As três primeiras possuem códigos do Julius e devem ser
copiadas para a seguinte pasta do sistema operacional: <tt>\WINDOWS\system\\</tt>.
Já a última biblioteca é a API, propriamente dita, que deve ser incluı́da como
referência no código fonte do aplicativo.

O reconhecedor é inicializado através do construtor SREngine que recebe
como argumento o arquivo de configuração do Julius ("jConf"). Esse arquivo
contém as especificações de todos os recursos utilizados pelo Julius durante o
processo de reconhecimento de voz, como os modelos acústico e de linguagem. O
modelo acústico independente de locutor foi construı́do com 
[recursos próprios](http://docplayer.com.br/1676961-Novos-recursos-e-utilizacao-de-adaptacao-de-locutor-no-desenvolvimento-de-um-sistema-de-reconhecimento-de-voz-para-o-portugues-brasileiro.html),
usando arquivos de áudio re-amostrados para 22,050 Hz com 16 bits. A gramática
livre de contexto foi elaborada seguindo a documentação do Julius. Mais 
detalhes sobre o arquivo de configuração podem ser encontrados na Seção 5.

```c
SREngine re;
re = new SREngine("C:/PPTController/pptConf/ppt.jconf");
```

Uma forma alternativa de especificar a gramática livre de contexto a ser 
utilizada durante o processo de reconhecimento é através do método
<tt>loadGrammar()</tt>.  Para isso, primeiramente, é preciso salvar o código 
SAPI XML abaixo em um arquivo (p.e. <tt>comandos.xml</tt>).

```xml
<?xml version="1.0" encoding="utf-8" ?>
<GRAMMAR LANGID="416">
<RULE NAME="COMMANDS">
    <LIST>
        <P>MOSTRAR</P>
        <P>ANTERIOR</P>
        <P>VOLTAR</P>
        <P>SAIR</P>
        <P>PRIMEIRO</P>
        <P>FECHAR</P>
    </LIST>
</RULE>
```

Em seguida, basta utilizar o método <tt>loadGrammar()</tt>, tendo como parâmetro 
de entrada o código SAPI XML. No momento, não é permitido o uso de caracteres
especiais no arquivo XML.

```c
re.loadGrammar("comandos.xml");
```

Lembrando que o método <tt>addGrammar()</tt> aceita como entrada os arquivos que 
formam a gramática de reconhecimento do Julius. Ou seja, o código abaixo tem o 
mesmo efeito do método <tt>loadGrammar()</tt> já citado, a diferença é que a 
gramática está no formato do Julius.

```c
re.addGrammar("comandos", "comandos.dict", "comandos.dfa");
```

Todos os arquivos de gramática testados no PPTController podem ser en-
contrados na pasta <tt>/PPTController/pptGrammar/</tt>.

Com o reconhecedor instanciado deve-se delegar para o <tt>SREngine</tt> quais 
funções vão representar os eventos, ou seja, o que acontecerá quando o 
reconhecimento for ativado ou quando alguma sentença for reconhecida, eventos 
<tt>OnSpeechReady</tt> e <tt>OnRecognition</tt>, respectivamente.

```c
SREngine.OnRecognition += sr_onRecognition;
re.OnSpeechReady += sr_speechReady;
```

Ao evento <tt>OnSpeechReady</tt> deve ser passada uma função sem argumentos,
informando que o Julius está ativado para reconhecimento. Já o evento 
<tt>OnRecognition</tt> recebe uma função com o argumento RecoResult. A 
<tt>RecoResult</tt>
é uma classe da API produzida sempre que uma sentença é reconhecida. Ela 
possui informações sobre o reconhecimento, como a própria sentença e o seu nı́vel
de confiança informado pelo reconhecedor. O uso das funções pode ser conferido
no código abaixo.

```c
void sr_onRecognition(RecoResult result){
    if (result.getConfidence() > 0.7)
        Actions(result.getUterrance());
}
void sr_speechReady(){
    recogetionRichTextBox1.AppendText("Reconhecendo\n");
}
```

A função Actions, presente no código acima, é responsável por controlar as
funcionalidades do programa Microsoft PowerPoint. Para isso, ela faz uso de
componentes integrados dentro de uma arquitetura _object linking and embedding_
(OLE). Essa comunicação não será aqui documentada, porém seu código é de
fácil compreensão e encontra-se disponı́vel no pacote do PPTController.

Assumindo que o reconhecedor está instanciado, o método <tt>startRecognition()</tt>
pode ser invocado para iniciar o processo de reconhecimento, assim como o
método <tt>stopRecognition()</tt> para parar o reconhecimento. No PPTController, 
especificamente, esses métodos são chamados a partir de ações de dois botões
mostrados abaixo.

```c
void button1_Click(object sender, EventArgs e) {
    re.startRecognition();
}
void button2_Click(object sender, EventArgs e) {
    re.stopRecognition();
}
```

O aplicativo desenvolvido, PPTContoller, é um _software_ livre e encontra-se
disponı́vel na página do Projeto FalaBrasil [2], juntamente com as versões mais
atualizadas da API e dos recursos construı́dos.

## Condições de Instalação e Operação
A API foi originalmente projetada para o sistema operacional Microsoft Windows 
32 bits (x86). A Tabela abaixo mostra as configurações recomendadas para o
correto funcionamento da API.

| Caracterı́sticas                  | Especificação                       |
|:--------------------------------:|:-----------------------------------:|
| Espaço em disco rı́gido           | 50 MB                               |
| Memória RAM                      | 2 GB                                |
| Sistema operacional              | Windows XP, Vista ou 7              |
| Processador                      | Intel(R) Pentium Dual Core 1.80GHz  |
| Placa de som                     | 16-bits                             |
| Hardware especial                | Microfone                           |
| PPTController: software especial | Microsoft Office PowerPoint 2007    |

Contudo, uma versão da API para ambiente Linux 32-bit também foi desenvolvida. 
Seu princı́pio de funcionamento é o mesmo da versão para Windows,
descrita na Seção 1. A diferença básica é que as ações são passadas para a API
através de ponteiro de função, usando o método <tt>setOnRecognizeAction()</tt>.

```c
void reconheceu(RecoResult *result){
    cout << result->getUterrance() << " Confiança " << result->getConfidence() << endl;
}
re->setOnRecognizeAction(&reconheceu);
```

Para usar a biblioteca compartilhada (<tt>libLapsAPI.so</tt>) deve-se copiá-la 
para <tt>/usr/lib</tt> e passá-la como parâmetro para o compilador. O código 
abaixo mostra como compilar a função main.


```bash
$ gcc -I LapsAPI/include/ \
        -I julius-4.1.3/libjulius/include/ \
        -I julius-4.1.3/libsent/include/ \
        -o main main.cpp \
        -L LapsAPI/Release/ \
        -l LapsAPIJulius4.1.3

main.cpp: In function int main():
main.cpp:15: warning: deprecated conversion from string constant to char*
```

Além do compilador <tt>gcc</tt> (versão 4.2.4), é fundamental que o compilador
<tt>g++</tt> (versão 4.2.4) também esteja instalado na máquina. O compilador 
<tt>g++</tt>
é necessário para manipular os dados da biblioteca. Caso não se tenha acesso
a pasta de bibliotecas, pode-se sobrescrever a variável <tt>LD_LIBRARY_PATH</tt>,
através do comando <tt>export</tt>.

```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"caminho/para/libLapsAPI.so"
```

## Extras
Um erro comum de compilação do programa PPTController é o endereçamento
errado das referências. Dito isso, as DLL’s abaixo precisam estar adicionadas
ao projeto:

- _LapsAPI_ : encontrada na pasta <tt>/LaPSAPI/</tt> presente no pacote da API.
- _Microsoft.Office.Core_ e _Microsoft.Office.Interop.PowerPoint_: por exemplo, para adicionar as DLL’s no ambiente de programação Microsoft Visual Studio, siga os passos abaixo:
    1. Click com o botão direito no item _References_ presente na janela _Solution Explorer_, que pode ser visualizada na Figura abaixo;
    2. Seleciona a opção _Add References_;
    3. Na janela que se segue, dentro da aba COM, procure e selecione o
objeto _Microsoft PowerPoint 12.0 Object Library_.


![Exemplo da janela Solution Explorer](./doc/solution_explorer.png)

O arquivo de configuração para executar o PPTController pode ser encon trado na 
pasta <tt>/PPTController/pptConf/</tt>. Caso ocorra algum erro durante o
carregamento das configurações, verifique o endereçamento dos modelos acústico
e de linguagem dentro do arquivo de configuração, pois dependendo do diretório
que o sistema tome como base para a aplicação, será preciso alterar o caminho
dos arquivos abaixo:

```bash
################### grammar path #######################
-dfa "C:/Coruja0.2/Gramatica/comandos.dfa"
-v "C:/Coruja0.2/Gramatica/comandos.dict"
################ acoustic model path ###################
-h "C:/Coruja0.2/LaPSAM1.5/LaPSAM1.5.am.bin"
-hlist "C:/Coruja0.2/LaPSAM1.5/tiedlist1.5.txt"
-htkconf "C:/Coruja0.2/LaPSAM1.5/edaz.conf"
```

O arquivo de log <tt>/PPTController/pptConf/JuliusLog</tt> pode ajudar a 
encontrar a origem do erro.

[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2019)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
