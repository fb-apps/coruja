package br.ufpa.laps.jlapsapi.recognizer;

//import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader; //import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.zip.ZipException;

import javax.speech.EngineException; //import javax.speech.recognition.RuleGrammar;
//import javax.speech.recognition.DictationGrammar;

//import com.sun.speech.engine.recognition.BaseDictationGrammar;
//import com.sun.speech.engine.recognition.BaseRuleGrammar;

import br.ufpa.laps.jlapsapi.jsapi.JLaPSAPIResultListener;
import br.ufpa.laps.jlapsapi.util.ConfigurationManager;
import br.ufpa.laps.jlapsapi.util.JarOrProjectRunnerSelector;
import br.ufpa.laps.jlapsapi.util.LibNameLoader;
import br.ufpa.laps.jlapsapi.util.ZipExtractor;
import br.ufpa.laps.jlapsapi.util.conversor.Conversor;
import br.ufpa.laps.jlapsapi.util.MkdfaRunner;

public class Recognizer {

	private static final String JULIUS_CONFIGURATION_FILE_PATH_DEFAULT = ConfigurationManager.DEFAULT_JULIUS_CONFIGURATIONS_FILE_PATH;

	private static final String JULIUS_CONFIGURATION_FILE_PATH_ALTERNATIVE = ConfigurationManager.ALTERNATIVE_JULIUS_CONFIGURATIONS_FILE_PATH;

	private static String JULIUS_CONFIGURATION_FILE_PATH = JULIUS_CONFIGURATION_FILE_PATH_DEFAULT;

	private JLaPSAPIResultListener resultListener;

	private Result result = new Result();

	private native void startSREngine(String configFilePath);

	private native void stopSREngine();

	private native void enableDictation(boolean enableDictation);// Dictation

	// Recognition

	private native void enableGrammarDictation(boolean enableGrammarDictation,
			String grammarName);

	private native void loadGrammar(String nameGrammar, String filegrammar);

	public Recognizer() {
		// System.loadLibrary(getLibName());
	}

	public void allocate() throws EngineException {
		// Extrai arquivos necessários do .jar. São eles: "gramtools" e
		// "bibliotecas nativas"

		final URI uri;
		final String appPath, jarLibPath;

		// Extração de arquivos
		try {
			JarOrProjectRunnerSelector joprs = new JarOrProjectRunnerSelector();
			uri = ZipExtractor.getJarURI();

			// Put "jlapsapiLibPath" and "gramtools" folder inside application
			// path
			//appPath = uri.toString().substring(5,uri.toString().substring(5).length()-12);
			appPath = joprs.getAppPath();
			//appPath = uri.toString().substring(5,
					//uri.toString().substring(5).length() - 8);
			//jarLibPath = appPath + "/jlapsapiLibPath";
			jarLibPath = joprs.getJarLibPath();

			// execute as jar
			//if (!uri.toString().endsWith("/jlapsapi/bin/")) {
			if (joprs.isJar()) {
				File fileJar = new File(uri.toString().substring(5));

				// LINHA PARA TESTAR COMO .JAR, TEM QUE POR O jlapsapi.jar na
				// pasta bin do projeto
				// File fileJar = new
				// File(uri.toString().substring(5)+"/jlapsapi.jar");

				// Extract files on application execution path
				File fileOut = new File(appPath);
				
				// só extrai as pastas gramtools e jlapsapiLibPath
				ZipExtractor.extrairZip(fileJar, fileOut);

				// Extracting Libs and setting permissions
				fileOut = new File(jarLibPath + "/libcoruja.so");
				// fileOut.setExecutable(true, true);
				//fileOut = new File(jarLibPath + "/libjcoruja.so");
				// fileOut.setExecutable(true, true);

				// Extracting gramtools and setting permissions
				fileOut = new File(appPath + "/gramtools/mkdfa.pl");
				fileOut.setExecutable(true, true);
				fileOut = new File(appPath + "/gramtools/mkfa");
				fileOut.setExecutable(true, true);
				fileOut = new File(appPath + "/gramtools/dfa_minimize");
				fileOut.setExecutable(true, true);

				// Load coruja
				System.load(LibNameLoader.libRoot(jarLibPath));

			} else {// execute on eclipse	
				
				//String projectLibPath = uri.toString().substring(5,uri.toString().substring(5).length())+ "/jlapsapiLibPath";
				String projectLibPath = joprs.getProjectLibPath();
				System.load( LibNameLoader.libRoot(projectLibPath) );

			}
			
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ZipException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Alocação do reconhecedor
		try {

			File file = new File(JULIUS_CONFIGURATION_FILE_PATH);
			if (!file.exists()) {
				JULIUS_CONFIGURATION_FILE_PATH = JULIUS_CONFIGURATION_FILE_PATH_ALTERNATIVE;
			}

			FileInputStream in = new FileInputStream(
					JULIUS_CONFIGURATION_FILE_PATH);
			in.close();

			startSREngine(JULIUS_CONFIGURATION_FILE_PATH);
		} catch (FileNotFoundException exception) {
			throw new EngineException(
					"ERROR: Could not find julius configuration file on paths "
							+ JULIUS_CONFIGURATION_FILE_PATH_DEFAULT + " or "
							+ JULIUS_CONFIGURATION_FILE_PATH_ALTERNATIVE);
		} catch (IOException exception) {
			throw new EngineException("ERROR: " + exception);
		}
	}

	public void deallocate() {
		stopSREngine();
	}

	public void resume() {
		enableDictation(true);
	}

	public void pause() {
		enableDictation(false);
	}

	public void addResultListener(JLaPSAPIResultListener resultListener) {
		this.resultListener = resultListener;
	}

	@SuppressWarnings("unused")
	private void newResultCallback(String utterance) {
		result.setUtterance(utterance);
		// resultListener.resultAccepted(result);
	}

	@SuppressWarnings("unused")
	private void newConfidenceResultCallback(float confidence) {
		// System.out.println("*jlapsapi confidence: "+confidence);
		result.setConfidence(confidence);
		resultListener.resultAccepted(result);
	}

	public String loadJSGF(Reader file) throws IOException {
		System.out.println("*jlapsapi Recognizer loadJSGF");

		// create dir for julius grammar files
		String tempSO = System.getProperty("java.io.tmpdir");
		File dir = new File(tempSO + "/jlapsapi/dirgram");
		if (dir.mkdirs())
			System.out.println("Diretório criado em: " + dir.getAbsolutePath());

		// Generate julius grammar files
		String grammarName = Conversor.Converter((FileReader) file, tempSO
				+ "/jlapsapi/dirgram/");

		// Compile grammar
		MkdfaRunner.execMkdfapl(tempSO + "/jlapsapi/dirgram/" + grammarName);

		// callMkdfapl(tempSO+"/jlapsapi/dirgram/"+grammarName);
		loadGrammar(grammarName, tempSO + "/jlapsapi/dirgram/" + grammarName);

		// returns the grammarName written inside JSAPI grammar file
		return grammarName;
	}

	public void setEnabled(boolean enableGrammarDictation, String grammarName) {
		// active or deactive Search on lapsapi by name
		enableGrammarDictation(enableGrammarDictation, grammarName);
	}
}