package br.ufpa.laps.jlapsapi.recognizer;

public class Result {
	
	private String utterance;
	private float confidence;

	public String getUtterance() {
		return utterance;
	}
	
	public float getConfidence() {
		return confidence;
	}
	
	public void setUtterance(String utterance) {
		this.utterance = utterance;
	}
	
	public void setConfidence(float confidence) {
		this.confidence = confidence;
	}
	
	
}
