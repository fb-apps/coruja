package br.ufpa.laps.jlapsapi.util;

import java.net.URI;
import java.net.URISyntaxException;

public class JarOrProjectRunnerSelector {

	static URI uri;
	static String appPath, jarLibPath, projectLibPath;

	public JarOrProjectRunnerSelector() {
		// TODO Auto-generated constructor stub
		
		try {
			uri = ZipExtractor.getJarURI();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		appPath = uri.toString().substring(5,uri.toString().substring(5).length()-12);
		jarLibPath = appPath + "/jlapsapiLibPath";
		projectLibPath = uri.toString().substring(5,uri.toString().substring(5).length())+ "/jlapsapiLibPath";
	}

	public boolean isJar(){		

		if (!uri.toString().endsWith("/jlapsapi/bin/")){
			return true;
		}

		return false;
	}

	public String getAppPath(){
		return appPath;
	}
	
	public String getJarLibPath(){
		return jarLibPath;
	}

	public String getProjectLibPath(){
		return projectLibPath;
	}
}
