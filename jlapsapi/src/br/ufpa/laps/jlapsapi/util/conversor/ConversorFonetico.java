package br.ufpa.laps.jlapsapi.util.conversor;
import java.util.regex.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
//import java.io.FileNotFoundException;;

public class ConversorFonetico {
	public void detStress(boolean fonesp,String dicionario) throws IOException{

		//arquivo destino das palavras convertidas
		FileWriter tw = new FileWriter("./dicionariofonetico.txt"); 
		//arquivo contendo as vogais tônicas da palavras
		FileWriter svwl = new FileWriter("./vogaistonicas.txt");
		svwl.write("AVISO --> Em caso de palavras hifenizadas, a vogal tonica encontra-se na palavra mais a direita.\n");
		//dicionário para conversão
		/*FileInfo theSourceFile = new FileInfo ("./.convert.dic");
		//dicionário para a identificação de vogal tônica 
		FileInfo theSourceFile2 = new FileInfo("./.stress.dic");
		//Dicionario
		FileInfo theSourceFile3 = new FileInfo ("./"+dicionario);*/
								
		BufferedReader reader = new BufferedReader(new FileReader("./convert.dic"));
		BufferedReader reader2 = new BufferedReader(new FileReader("./stress.dic"));
		BufferedReader format = new BufferedReader(new FileReader("./"+dicionario));
		String word = reader.readLine();
		String word2 = reader2.readLine();
		String grafema = format.readLine();
		String word2_aux = "";
		String word2_aux2 = "";
		String word_aux = "";
		String word_aux2 = "";
		String palavra = "";
		int contador = 0;
		int cont_piece = 0;
		int pos = 0;
		boolean find = false;
		String strVw = "";
		int ind_svw = 0;
		String transc = "";
		String letra = "";
		String form_grafema = "";
		//int sem = 0;
		int pula2 = 0;
		int pula = 0;	
		String desvio = "";
		int n = 0;
		int count= 0;

		String [] exceptions = {"#táxis#","#táxi#","#taxista#","#taxistas#","#taxi#","#taxidermia#","#taximétrica#","#taxímetro#","#oxítono#","#oxítona#","#oxítonas#","#oxítonos#","#proparoxítona#","#proparoxítonas#","#paroxítona#","#paroxítonas#","#reflexo#","#intoxicação#","#desintoxicação#","#desintoxicado#"};
		
		String [] exceptions_o = {"#maior#","#maiores#","#menor#","#menores#","#melhores#","#melhor#","#pior#","#piores#","#suor#","#suores#"};
		
		while (word != null){
			n = 0;
			pos = 0;
			find = false;
			strVw = "";
			ind_svw = 0;
			palavra = "";
			transc = "";
			pula2 = 0;
			cont_piece = 0;
			form_grafema = "#" + grafema + "#######";

			//Regex rule_00 = new Regex("(-)");
			Pattern rule_00 = Pattern.compile("(##)");
			Matcher m00 = rule_00.matcher(word2);
			System.out.println(m00.find());
			if(m00.find()){
				String [] piece = word2.split("##");
				//Console.WriteLine("{0} palavras",piece.Length);
				if(piece.length == 2){
					word2 = piece[0] + "#";
					word2_aux = "#" + piece[1];
					word = piece[0] + "######";
					word_aux = "#" + piece[1] + "######";
				}
				else {
					if(piece.length == 3){
						word2 = piece[0] + "#";
						word2_aux = "#" + piece[1] + "#";
						word2_aux2 = "#" + piece[2];
						word = piece[0] + "######";
						word_aux = "#" + piece[1] + "######";
						word_aux2 = "#" + piece[2] + "######";
					}
				}
			}
			
			//Rotina de identific//arquivo destino das palavras convertidas
		/*TextWriter tw = new StreamWriter("./dicionariofonetico.txt"); 
		//arquivo contendo as vogais tônicas da palavras
		TextWriter svwl = new StreamWriter("./vogaistonicas.txt");
		svwl.WriteLine("AVISO --> Em caso de palavras hifenizadas, a vogal tonica encontra-se na palavra mais a direita.");
		//dicionário para conversão
		FileInfo theSourceFile = new FileInfo ("./.convert.dic");
		//dicionário para a identificação de vogal tônica 
		FileInfo theSourceFile2 = new FileInfo("./.stress.dic");
		//Dicionario
		FileInfo theSourceFile3 = new FileInfo ("./"+dicionario);*/
								
		/*StreamReader reader = theSourceFile.OpenText();
		StreamReader reader2 = theSourceFile2.OpenText();
		StreamReader format = theSourceFile3.OpenText();*/
		word = reader.readLine();
		System.out.println(word);
		word2 = reader2.readLine();
		grafema = format.readLine();
		word2_aux = "";
		word2_aux2 = "";
		word_aux = "";
		word_aux2 = "";
		palavra = "";
		contador = 0;
		cont_piece = 0;
		pos = 0;
		find = false;
		strVw = "";
		ind_svw = 0;
		transc = "";
		letra = "";
		form_grafema = "";
		pula2 = 0;
		pula = 0;	
		String volta = "";
		String [] letter2 = new String [/*word2.length()*/200];

		//exceptions = {"#táxis#","#táxi#","#taxista#","#taxistas#","#taxi#","#taxidermia#","#taximétrica#","#taxímetro#","#oxítono#","#oxítona#","#oxítonas#","#oxítonos#","#proparoxítona#","#proparoxítonas#","#paroxítona#","#paroxítonas#","#reflexo#","#intoxicação#","#desintoxicação#","#desintoxicado#"};
		
		//exceptions_o = {"#maior#","#maiores#","#menor#","#menores#","#melhores#","#melhor#","#pior#","#piores#","#suor#","#suores#"};

		while (word != null)
		{
			count = 0;
			pos = 0;
			find = false;
			strVw = "";
			ind_svw = 0;
			palavra = "";
			transc = "";
			pula2 = 0;
			cont_piece = 0;
			form_grafema = "#" + grafema + "#######";
			System.out.println("Passou por aqui");

			//Regex rule_00 = new Regex("(-)");
			
			rule_00 = Pattern.compile("(##)");
			m00 = rule_00.matcher(word2);
			if(m00.find())
			{
				String [] piece = word2.split("##");
				//Console.WriteLine("{0} palavras",piece.Length);
				if(piece.length == 2){
					word2 = piece[0] + "#";
					word2_aux = "#" + piece[1];
					word = piece[0] + "######";
					word_aux = "#" + piece[1] + "######";
				}
				else
				{
					if(piece.length == 3){
						word2 = piece[0] + "#";
						word2_aux = "#" + piece[1] + "#";
						word2_aux2 = "#" + piece[2];
						word = piece[0] + "######";
						word_aux = "#" + piece[1] + "######";
						//ação de vogal tônica
			//volta:
			do{
			volta = "";
			//String [] letter2 = new String [word2.length()];
			String [] aux = new String [word2.length()];
			

			while (find == false)
			{						
				//Regra 1
				Pattern rule_1 = Pattern.compile("([áéíóúâêôàè])");
				Matcher m = rule_1.matcher(word2);
				if(m.find())
				{			
					find = true;strVw = m.toString();ind_svw = m.start();break;
				}
				else
				{	
					Pattern rule_0 = Pattern.compile("([^#][rlzxn][#])");
					Matcher m0 = rule_0.matcher(word2);
					if(m0.find())
					{	
				  	pos = m0.start();
						if(word2.substring(pos+1,pos+2) == "r")
						{
							if (word2.substring(pos,pos+1) == "a"|word2.substring(pos,pos+1) == "e"|word2.substring(pos,pos+1) == "i"|word2.substring(pos,pos+1) == "o"|word2.substring(pos,pos+1) == "u")
							{
								find = true;
								strVw = word2.substring(pos,pos+1);
								ind_svw = m0.start();
								break;
							}
							else
							{
								strVw = "nf";
								break;
							}	
						}
						if(word2.substring(pos+1,pos+1) == "l")
						{
							if (word2.substring(pos,pos+1) == "a"|word2.substring(pos,pos+1) == "e"|word2.substring(pos,pos+1) == "i"|word2.substring(pos,pos+1) == "o"|word2.substring(pos,pos+1) == "u")
							{
								find = true;
								strVw = word2.substring(pos,pos+1);
								ind_svw = m0.start();
								break;
							}
							else
							{
								strVw = "nf";
								break;
							}	
						}
						if(word2.substring(pos+1,1) == "z")
						{
							if (word2.substring(pos,pos+1) == "a"|word2.substring(pos,pos+1) == "e"|word2.substring(pos, pos+1) == "i"|word2.substring(pos, pos+1) == "o"|word2.substring(pos, pos+1) == "u")
							{
								find = true;
								strVw = word2.substring(pos,pos+1);
							  	ind_svw = m0.start();
								break;
							}
							else
							{
									strVw = "nf";
									break;
							}	
						}
						if(word2.substring(pos+1,1) == "x")
						{
							if (word2.substring(pos, pos+1) == "a"|word2.substring(pos, pos+1) == "e"|word2.substring(pos, pos+1) == "i"|word2.substring(pos, pos+1) == "o"|word2.substring(pos, pos+1) == "u")
							{
								find = true;
								strVw = word2.substring(pos, pos+1);
								ind_svw = m0.start();
								break;
							}
							else
							{
								strVw = "nf";break;
							}	
						}
						if(word2.substring(pos+1,1) == "n")
						{
							if (word2.substring(pos, pos+1) == "a"|word2.substring(pos, pos+1) == "e"|word2.substring(pos, pos+1) == "i"|word2.substring(pos, pos+1) == "o"|word2.substring(pos, pos+1) == "u")
							{
								find = true;
								strVw = word2.substring(pos, pos+1);
								ind_svw = m0.start();
								break;
							}
							else
							{
								strVw = "nf";break;
							}	
						}
					}
					else
					{ 
						//Regra 1 – precedência de acentos
						Pattern rule_1_til = Pattern.compile("([ãõ])");
						Matcher m1til = rule_1_til.matcher(word2);
						if(m1til.find())
						{	
							pos = m1til.start();ind_svw = m1til.start();
							find = true;strVw = word2.substring(pos, pos+1);break;
						}
						//Regra 2
						Pattern rule_21 = Pattern.compile("(([#][aeo][#])|([#][aeiou][^aeiou][#]))");
						Matcher m21 = rule_21.matcher(word2);
						if(m21.find())
						{	
							pos = m21.start();ind_svw = pos+1;
							find = true;strVw = word2.substring(pos+1,1);break;
						}
						Pattern rule_212 = Pattern.compile("(([#][^aeiou][aeiou][#])|([#][^aeiou][aeiou][^aeiou][#]))");
						Matcher m212 = rule_212.matcher(word2);
						if(m212.find())
						{	
							pos = m212.start();ind_svw = pos+2;
							find = true;strVw = word2.substring(pos+2,pos+3);break;
						}
						Pattern rule_213 = Pattern.compile("([#][^aeiou][^aeiou][aeiou][^aeiou][#])");
						Matcher m213 = rule_213.matcher(word2);
						if(m213.find())
						{	
							pos = m213.start();ind_svw = pos+3;
							find = true;strVw = word2.substring(pos+3,pos+4);break;
						}
						//Regra 3												
						Pattern rule_3 = Pattern.compile("([iou]m#)");
						Matcher m3 = rule_3.matcher(word2);
						if(m3.find())
						{
							pos = m3.start();ind_svw = pos;
							find = true;strVw = word2.substring(pos, pos+1);break;
						}
						//Regra 4 
						Pattern rule_4 = Pattern.compile("([iou]ns#)");//Regra 1 preced?ncia
						Matcher m4 = rule_4.matcher(word2);
						if(m4.find())
						{	
							pos = m4.start();ind_svw =pos;  //word2.Length-4;
							find = true;strVw = word2.substring(pos, pos+1);break;
						}
						//Regra 5 
						Pattern rule_5 = Pattern.compile("[qg][u][i]#");
						Matcher m5 = rule_5.matcher(word2);
						if(m5.find())
						{ 
							pos = m5.start();ind_svw = pos+2;//word2.Length-2;
							find = true;strVw = word2.substring(pos+2,pos+3);break;
						}
						//Regra 6
						Pattern rule_6 = Pattern.compile("[qg][u][i]s#");
						Matcher m6 = rule_6.matcher(word2);
						if(m6.find())
						{
							pos = m6.start();ind_svw = pos+2;//word2.Length-2;
							find = true;strVw = word2.substring(pos+2,pos+3);break;
						}
						//Regra 7   
						Pattern rule_7 = Pattern.compile("[aeiou][iu]#");
						Matcher m7 = rule_7.matcher(word2);
						if(m7.find())
						{ 

							pos = m7.start();ind_svw = pos;//word2.Length-3;
							find = true;strVw = word2.substring(pos, pos+1);break;
						}
						//Regra 8   
						Pattern rule_8 = Pattern.compile("[^aeiou][iu]#");
						Matcher m8 = rule_8.matcher(word2);
						if(m8.find())
						{ 
							pos = m8.start();ind_svw = pos+1;//word2.Length-2;
							find = true;strVw = word2.substring(pos+1,pos+2);break;
						}
						//Regra 9   
						Pattern rule_9 = Pattern.compile("[aeiou][iu]s#");
						Matcher m9 = rule_9.matcher(word2);
						if(m9.find())
						{
							pos = m9.start();ind_svw = pos;//word2.Length-4;
							find = true;strVw = word2.substring(pos, pos+1);break;
						}
						//Regra 10   
						Pattern rule_10 = Pattern.compile("[^aeiou][iu]s#");
						Matcher m10 = rule_10.matcher(word2);
						if(m10.find())
						{
							pos = m10.start();ind_svw = pos+1;//word2.Length-3;
							find = true;strVw = word2.substring(pos+1,pos+2);break;
						}
						//Regra 11   
						Pattern rule_11 = Pattern.compile("[p][o][r][q][u][e][#]");
						Matcher m11 = rule_11.matcher(word2);
						if(m11.find())
						{
							pos = m11.start();ind_svw = pos+6;//word2.Length-2;
							find = true;strVw = word2.substring(pos+6,pos+7);break;
						}							
						//Regra 12   
						Pattern rule_12 = Pattern.compile("[aeiou]que#");
						Matcher m12 = rule_12.matcher(word2);
						if(m12.find())
						{
							pos = m12.start();ind_svw = pos;//word2.Length-5;
							find = true;strVw = word2.substring(pos, pos+1);break;
						}
						//Regra 13
						Pattern rule_13 = Pattern.compile("[aeiou]ques#");
						Matcher m13 = rule_13.matcher(word2);
						if(m13.find())
						{
							pos = m13.start();ind_svw = pos;//word2.Length-6;
							find = true;strVw = word2.substring(pos, pos+1);break;
						}
						//Regra 14   
						Pattern rule_14 = Pattern.compile("[aeiou][iu][aeiou]#");
						Matcher m14 = rule_14.matcher(word2);
						if(m14.find())
						{
							pos = m14.start();ind_svw = pos;//word2.Length-4;
							find = true;strVw = word2.substring(pos, pos+1);break;
						}
						//Regra 15   
						Pattern rule_15 = Pattern.compile("([^qg][aeiou][iu][^aeiou][aeiou][#])");
						Matcher m15 = rule_15.matcher(word2);
						if(m15.find())
						{
							pos = m15.start();ind_svw = pos+1;//word2.Length-5;
							find = true;strVw = word2.substring(pos+1,1);break;
						}
						//Regra 16   
						Pattern rule_16 = Pattern.compile("([aeiou][iu][^aeiou][aeiou][s][#])");
						Matcher m16 = rule_16.matcher(word2);
						if(m16.find())
						{
							pos = m16.start();ind_svw = pos;//word2.Length-6;
							find = true;strVw = word2.substring(pos, pos+1);break;
						}
						//Regra 17   
						Pattern rule_17 = Pattern.compile("([aeiou][iu][n][^aeiou][aeo][#])");
						Matcher m17 = rule_17.matcher(word2);
						if(m17.find())
						{
							pos = m17.start();ind_svw = pos+1;//word2.Length-5;	
							find = true;strVw = word2.substring(pos+1,pos+2);break;
						}
						//Regra 18
						Pattern rule_18 = Pattern.compile("[^qg][aeiou][iu][^aeiou]");
						Matcher m18 = rule_18.matcher(word2);
						if(m18.find() & word2 != "#feijoada#" & word2 != "#caipirinha#" & word2 != "#caipirinhas#" & word2 != "#saudade#" & word2 != "#saudades#")
						{
							pos = m18.start();ind_svw = pos+1;//word2.Length-3;
							find = true;strVw = word2.substring(pos+1,pos+2);break;
						}
						//Regra 19 
						Pattern rule_19 = Pattern.compile("[q][u][e][m][#]");
						Matcher m19 = rule_19.matcher(word2);
						if(m19.find())
						{	
							pos = m19.start();ind_svw = pos+2;//word2.Length-3;
							find = true;strVw = word2.substring(pos+2,pos+3);break;
						}
						//Regra 19 direcionada à ‘que’
						Pattern rule_19q = Pattern.compile("[q][u][e][#]");
						Matcher m19q = rule_19q.matcher(word2);
						if(m19q.find())
						{	
							pos = m19q.start();ind_svw = pos+2;//word2.Length-2;
							find = true;strVw = word2.substring(pos+2,pos+3);break;
						}
						if (find == false)
						{
							//Regra 20
							Pattern rule_202 = Pattern.compile("([aeiou][aeiou])#");
							Matcher m202 = rule_202.matcher(word2);
							if(m202.find())
							{	
								pos = m202.start();ind_svw = pos;//word2.Length-3;
								find = true;strVw = word2.substring(pos, pos+1);break;
							}
							else
							{ 
								Pattern rule_203 = Pattern.compile("(([aeiou][^aeiou][aeiou])|([aeiou][aeiou][^aeiou]))#");
								Matcher m203 = rule_203.matcher(word2);
								if(m203.find())
								{	
									pos = m203.start();ind_svw = pos;//word2.Length-4;
									find = true;strVw = word2.substring(pos, pos+1);break;
								
								}else
								{
									Pattern rule_204 = Pattern.compile("(([aeiou][^aeiou][^aeiou][aeiou])|([aeiou][^aeiou][aeiou][^aeiou]))#");
									Matcher m204 = rule_204.matcher(word2);
									if(m204.find())
									{	
										pos = m204.start();ind_svw = pos;//word2.Length-5;
										find = true;strVw = word2.substring(pos, pos+1);break;
									}
									else
									{
										Pattern rule_205 = Pattern.compile("(([aeiou][^aeiou][^aeiou][aeiou][^aeiou])|([aeiou][^aeiou][^aeiou][^aeiou][aeiou])|([aeiou][^aeiou][aeiou][^aeiou][^aeiou]))#");
										Matcher m205 = rule_205.matcher(word2);
										if(m205.find())
										{	
											pos = m205.start();ind_svw = pos;// word2.Length-6;
											find = true;strVw = word2.substring(pos, pos+1);break;
										
										}
										else
										{
											Pattern rule_206 = Pattern.compile("(([aeiou][^aeiou][^aeiou][^aeiou][aeiou][^aeiou]))#");
											Matcher m206 = rule_206.matcher(word2);
											if(m206.find())
											{	
												pos = m206.start();ind_svw = pos;//word2.Length-7;
												find = true;strVw = word2.substring(pos, pos+1);break;
												
											}
											else
											{
												//Console.WriteLine("{0} stress vowel not found!",word2);
												strVw = "nf";break;
											}
										}
									}
								}
							}
						}
					}
				}
			}
//Rotina de conversão g2p

			letra = "";
				pula = 0;
			//sair:
			do {
				desvio="";

			if(pula <= word2.length())
			{
				letra = form_grafema.substring(pula2,pula2+1);
				//Console.WriteLine("---> {0}",letra);
				if (letra == "-")
				{
					for (int j=1;j<word2.length();j++) 
					{
						palavra = palavra + letter2[j] + " ";
					}
					cont_piece = cont_piece + 1;
					if(cont_piece == 1){
						word = word_aux;
						word2 = word2_aux;
					}else{
						word = word_aux2;
						word2 = word2_aux2;
					}
					find = false;
						pula2 = pula2 + 1;
						volta = "volta";
					//goto volta;
				}
				if (desvio != "volta"){
			
				Pattern idSharp = Pattern.compile("#");
				Matcher gSharp = idSharp.matcher(letra);
				
				if(gSharp.find())
				{	
					if (pula == 0)
					{
						letter2[pula] = "$";
						pula = 1;
						pula2 = 1;
						desvio = "sair";
					}
					else{
						if (fonesp == true)
						{
							letter2[pula] = "sp";
							cont_piece = 0;
							desvio= "salvar";
							System.out.println("passou por aqui");
						}
					}
				}
				if(pula == 0 && desvio!="sair"){
					pula = 1;
					desvio = "sair";
				}
				if (desvio != "sair"){
				Pattern idA1 = Pattern.compile("a");
				Matcher gA1 = idA1.matcher(letra);
				if(gA1.find())
				{
					if (word.substring(pula+1,pula+3) == "n#"|word.substring(pula+1,pula+3) == "n-")
					{							
						letter2[pula] = "a~";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idA2 = Pattern.compile("a");
				Matcher gA2 = idA2.matcher(letra);
				if(gA2.find())
				{
					if (word.substring(pula+1,pula+3) == "m#"|word.substring(pula+1,pula+3) == "m-")
					{
						letter2[pula] = "a~ w~";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if(strVw == "a" && ind_svw == pula && desvio!="sair")
				{
					Pattern idA3 = Pattern.compile("a");
					Matcher gA3 = idA3.matcher(letra);
					if(gA3.find())
					{
						Pattern idA31 = Pattern.compile("[mn][haeiouáéíóúãâõê]"); 
						Matcher gA31 = idA31.matcher(word.substring(pula+1,pula+3));
						if(gA31.find())
						{
							letter2[pula] = "a~";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				if (desvio!="sair"){
				Pattern idA4 = Pattern.compile("â");
				Matcher gA4 = idA4.matcher(letra);
				if(gA4.find())
				{	
					Pattern idA41 = Pattern.compile("[mn]");
					Matcher gA41 = idA41.matcher(word.substring(pula+1,pula+2));
					if (gA41.find())
					{
						Pattern idA42 = Pattern.compile("[^haeiouáéíóúãâõê]");
						Matcher gA42 = idA42.matcher(word.substring(pula+2,pula+3));
						if (gA42.find())
						{
							letter2[pula] = "a~";
							pula = pula + 2;pula2 = pula2 + 2;
							desvio = "sair";
						}
					}
				}
				}
				if(desvio!="sair"){
				Pattern idA5 = Pattern.compile("ã");
				Matcher gA5 = idA5.matcher(letra);
				if(gA5.find())
				{	
					if (word.substring(pula+1,pula+2) == "o")
					{
						letter2[pula] = "a~ w~";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if(desvio!="sair"){
				Pattern idA6 = Pattern.compile("[ãâ]");
				Matcher gA6 = idA6.matcher(letra);
				if(gA6.find())
				{
					letter2[pula] = "a~";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idA7 = Pattern.compile("[àá]");
				Matcher gA7 = idA7.matcher(letra);
				if(gA7.find())
				{
					letter2[pula] = "a";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idA8 = Pattern.compile("a");
				Matcher gA8 = idA8.matcher(letra);
				if(gA8.find())
				{	
					Pattern idA81 = Pattern.compile("[mn]");
					Matcher gA81 = idA81.matcher(word.substring(pula+1,pula+2));
					if (gA81.find())
					{
						Pattern idA82 = Pattern.compile("[^haeiouáéíóúãâõê]");
						Matcher gA82 = idA82.matcher(word.substring(pula+2,pula+3));
						if (gA82.find())
						{
							letter2[pula] = "a~";
							pula = pula + 2;pula2 = pula2 + 2;
							desvio = "sair";
						}
					}
				}
				}
				if(desvio!="sair"){
				Pattern idA = Pattern.compile("a");
				Matcher gA = idA.matcher(letra);
				if(gA.find())
				{
					letter2[pula] = "a";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idB = Pattern.compile("b");
				Matcher gB = idB.matcher(letra);
				if(gB.find())
				{	
					letter2[pula] = "b";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idC1 = Pattern.compile("c");
				Matcher gC1 = idC1.matcher(letra);
				if(gC1.find())
				{	
					Pattern idC11 = Pattern.compile("[êeiéí]");
					Matcher gC11 = idC11.matcher(word.substring(pula+1,pula+2));
					if(gC11.find())
					{
						letter2[pula] = "s";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idC2 = Pattern.compile("ç");
				Matcher gC2 = idC2.matcher(letra);
				if(gC2.find())
				{	
					aux[pula] = "C2";
					letter2[pula] = "s";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if(desvio!="sair"){
				Pattern idC3 = Pattern.compile("c");
				Matcher gC3 = idC3.matcher(letra);
				if(gC3.find())
				{	
					if (word.substring(pula+1,pula+2) == "h")
					{
						letter2[pula] = "S";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
					else
					{	
						letter2[pula] = "k";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){				
				Pattern idD1= Pattern.compile("d");
				Matcher gD1 = idD1.matcher(letra);
				if(gD1.find())
				{
					if (word.substring(pula+1,pula+2) == "i"|word.substring(pula+1,pula+4) == "es#"|word.substring(pula+1,pula+3) == "e#")
					{
						letter2[pula] = "dZ";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idD2= Pattern.compile("d");
				Matcher gD2 = idD2.matcher(letra);
				if(gD2.find())
				{
					Pattern idD22 = Pattern.compile("[^hrlaeiouáéíóúãâõê]");
					Matcher gD22 = idD22.matcher(word.substring(pula+1,pula+2));
					if (gD22.find())
					{
						letter2[pula] = "dZ";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idD3 = Pattern.compile("d");
				Matcher gD3 = idD3.matcher(letra);
				if(gD3.find())
				{
					if (word.substring(pula+1,pula+2) == "#")
					{			//sair:

						letter2[pula] = "dZ";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idD4 = Pattern.compile("d");
				Matcher gD4 = idD4.matcher(letra);
				if(gD4.find())
				{	
					letter2[pula] = "d";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idE1 = Pattern.compile("e");
				Matcher gE1 = idE1.matcher(letra);
				if(gE1.find())
				{
					Pattern idE11 = Pattern.compile("n");
					Matcher gE11 = idE11.matcher(word.substring(pula+1,pula+2));
					if (gE11.find())
					{
						if (word.substring(pula+2,pula+3) == "#"|word.substring(pula+1,pula+2) == "-")
						{
							letter2[pula] = "e~";
							pula = pula + 2;pula2 = pula2 + 2;
							desvio = "sair";
						}
					}					
				}
				}
				if (strVw == "e" & ind_svw == pula && desvio!="sair")
				{
					Pattern idE2 = Pattern.compile("e");
					Matcher gE2 = idE2.matcher(letra);
					if(gE2.find())
					{
						Pattern idE21 = Pattern.compile("l([^aeiouáéíóúãâõêh]|[#-])");
						Matcher gE21 = idE21.matcher(word.substring(pula+1,pula+3));
						if (gE21.find())
						{
							letter2[pula] = "E";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}	
					}
				}
				if (desvio!="sair"){
				Pattern idE3 = Pattern.compile("e");
				Matcher gE3 = idE3.matcher(letra);
				if(gE3.find())
				{	
					Pattern idE31 = Pattern.compile("l([^aeiouáéíóúãâõêh]|[#-])");
					Matcher gE31 = idE31.matcher(word.substring(pula+1,pula+3));
					if (gE31.find())
					{
						letter2[pula] = "e";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idE4 = Pattern.compile("e");
				Matcher gE4 = idE4.matcher(letra);
				if(gE4.find())
				{	
					Pattern idE41 = Pattern.compile("[ãõ]");
					Matcher gE41 = idE41.matcher(word.substring(pula-1,pula));
					if(gE41.find())
					{ 
						letter2[pula] = "j~";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idE5 = Pattern.compile("e");
				Matcher gE5 = idE5.matcher(letra);
				if(gE5.find())
				{	
					Pattern idE51 = Pattern.compile("a");
					Matcher gE51 = idE51.matcher(word.substring(pula-1,pula));
					if(gE51.find())
					{ 
						letter2[pula] = "j";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				if (strVw == "e"& ind_svw == pula)
				{
					if (word2 == "#este#"||word2 == "#estes#"||word2 == "#esse#"||word2 == "#esses#"||word2 == "#aquele#"||word2 == "#aqueles#"||word2 == "#seu#"||word2 == "#seus#"||word2 == "#teu#"||word2 == "#teus#"||word2 == "#ele#"||word2 == "#eles#"||word2 == "#dele#"||word2 == "#nele#"||word2 == "#neles#"||word2 == "#deles#")
					{
						Pattern idE6 = Pattern.compile("e");
						Matcher gE6 = idE6.matcher(letra);
						if(gE6.find())
						{
							Pattern idE61 = Pattern.compile("[s][t]|[s][s]|[l][a-z#]|u[a-z#]");
							Matcher gE61 = idE61.matcher(word.substring(pula+1,pula+3));
							if (gE61.find())
							{
								letter2[pula] = "e";
								pula = pula + 1;pula2 = pula2 + 1;
								desvio = "sair";
							}	
							
						}
					}
					else
					if (word2 == "#esta#"||word2 == "#estas#"||word2 == "#essa#"||word2 == "#essas#"||word2 == "#aquela#"||word2 == "#aquelas#"||word2 == "#ela#"||word2 == "#elas#"||word2 == "#dela#"||word2 == "#nela#"||word2 == "#nelas#"||word2 == "#delas#")
					{
						Pattern idE7 = Pattern.compile("e");
						Matcher gE7 = idE7.matcher(letra);
						if(gE7.find())
						{
							Pattern idE71 = Pattern.compile("[s][t]|[s][s]|[l][a-z#]|u[a-z#]");
							Matcher gE71 = idE71.matcher(word.substring(pula+1,pula+3));
							if (gE71.find())
							{
								letter2[pula] = "E";
								pula = pula + 1;pula2 = pula2 + 1;
								desvio = "sair";
							}	
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idE8 = Pattern.compile("e");
				Matcher gE8 = idE8.matcher(letra);
				if(gE8.find())
				{	
					if(word.substring(pula-1,1)=="#"|word.substring(pula+1,pula+2) == "-")
					{
						Pattern idE81 = Pattern.compile("x[aeiouáéíóúãâõê]");
						Matcher gE81 = idE81.matcher(word.substring(pula+1,pula+3));
						if (gE81.find())
						{
							letter2[pula] = "e";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
						else
						{
							Pattern idE9 = Pattern.compile("[eê][xs][b-d|f-h|j-n|p-t|v-z]");
							Matcher gE9 = idE9.matcher(word.substring(pula,pula+3));
							if(gE9.find())
							{	
								letter2[pula] = "e";
								pula = pula + 1;pula2 = pula2 + 1;
								desvio = "sair";
							}
							else
							{
								Pattern idE10 = Pattern.compile("e[^aeiouáéíóúãâõêmnx#]");
								Matcher gE10 = idE10.matcher(word.substring(pula,pula+2));
								if(gE10.find())
								{	
									letter2[pula] = "e";
									pula = pula + 1;pula2 = pula2 + 1;
									desvio = "sair";
								}
							}
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idEe11 = Pattern.compile("e");
				Matcher gEe11 = idEe11.matcher(letra);
				if(gEe11.find())
				{							
					Pattern idEe111 = Pattern.compile("[n|m]");
					Matcher gEe111 = idEe111.matcher(word.substring(pula+1,pula+2));
					if(gEe111.find())
					{ 
						Pattern idEe112 = Pattern.compile("(e#|e-)");
						Matcher gEe112 = idEe112.matcher(word.substring(pula+2,pula+4));
						if(gEe112.find())
						{
							letter2[pula] = "e";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idE12 = Pattern.compile("e");
				Matcher gE12 = idE12.matcher(letra);
				if(gE12.find())
				{	
					Pattern idE121 = Pattern.compile("l[ao]#");
					Matcher gE121 = idE121.matcher(word.substring(pula+1,pula+4));
					if (gE121.find())
					{
						if (word2 != "#pelo#"&word2 != "#pela#")	
						{	
							letter2[pula] = "E";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}	
				}
				}
				if (desvio!="sair"){
				Pattern idE13 = Pattern.compile("e");
				Matcher gE13 = idE13.matcher(letra);
				if(gE13.find())
				{	
					Pattern idE131 = Pattern.compile("([s][a]|[z][a])[#]");
					Matcher gE131 = idE131.matcher(word.substring(pula+1,pula+4));
					if(gE131.find())
					{ 
						letter2[pula] = "e";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
					else
					{
						Pattern idE132 = Pattern.compile("[s][s][a][#]");
						Matcher gE132 = idE132.matcher(word.substring(pula+1,pula+5));
						if(gE132.find())
						{ 
							letter2[pula] = "e";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idE14 = Pattern.compile("ê");
				Matcher gE14 = idE14.matcher(letra);
				if(gE14.find())
				{	
					Pattern idE141 = Pattern.compile("[mn][^haeiouáéíóúãâõê#]");
					Matcher gE141 = idE141.matcher(word.substring(pula+1,pula+3));
					if (gE141.find())
					{
						letter2[pula] = "e~";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}	
				}
				}
				if (desvio!="sair"){
				Pattern idE15 = Pattern.compile("e");
				Matcher gE15 = idE15.matcher(letra);
				if(gE15.find())
				{	
					Pattern idE151 = Pattern.compile("[mn][^haeiouáéíóúãôâõê#]");
					Matcher gE151 = idE151.matcher(word.substring(pula+1,pula+3));
					if (gE151.find())
					{
						letter2[pula] = "e~";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}	
				}
				}
				if (strVw == "e"&ind_svw == pula && desvio!="sair")
				{
					Pattern idE16 = Pattern.compile("e");
					Matcher gE16 = idE16.matcher(letra);
					if(gE16.find())
					{
						Pattern idE161 = Pattern.compile("[mn][^#]");
						Matcher gE161 = idE161.matcher(word.substring(pula+1,pula+3));
						if (gE161.find())
						{
							letter2[pula] = "e~";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}	
					}
				}
				if (desvio!="sair"){
				Pattern idE17 = Pattern.compile("[eéê]");
				Matcher gE17 = idE17.matcher(letra);
				if(gE17.find())
				{	
					Pattern idE171 = Pattern.compile("(m#|m-)");
					Matcher gE171 = idE171.matcher(word.substring(pula+1,pula+3));
					if(gE171.find())
					{ 
						letter2[pula] = "e~ j~";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idE18 = Pattern.compile("ê");
				Matcher gE18 = idE18.matcher(letra);
				if(gE18.find())
				{	
					letter2[pula] = "e";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idE19 = Pattern.compile("[éè]");
				Matcher gE19 = idE19.matcher(letra);
				if(gE19.find())
				{	
					letter2[pula] = "E";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idE20 = Pattern.compile("e");
				Matcher gE20 = idE20.matcher(letra);
				if(gE20.find())
				{	
					Pattern idE201 = Pattern.compile("(s#|s-)");
					Matcher gE201 = idE201.matcher(word.substring(pula+1,pula+3));
					if(gE201.find())
					{ 
						letter2[pula] = "i";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (strVw == "e"&ind_svw == pula && desvio!="sair")
				{
					Pattern idEe21 = Pattern.compile("e");
					Matcher gEe21 = idEe21.matcher(letra);
					if(gEe21.find())
					{
						Pattern idE121 = Pattern.compile("i");
						Matcher gE121 = idE121.matcher(word.substring(pula+1,pula+2));
						if (gE121.find())
						{
							letter2[pula] = "e j";
							pula = pula + 2;pula2 = pula2 + 2;
							desvio = "sair";
						}	
					}
				}
				if (ind_svw == pula+2 && desvio !="sair")
				{	
					Pattern idE22 = Pattern.compile("e");
					Matcher gE22 = idE22.matcher(letra);
					if(gE22.find())
					{
						Pattern idE221 = Pattern.compile("[a-z]strVw");
						Matcher gE221 = idE221.matcher(word.substring(pula+1,pula+3));
						if(gE221.find())
						{
							letter2[pula] = "e";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}	
					}
				}
				if (desvio!="sair"){
				Pattern idE23 = Pattern.compile("e");
				Matcher gE23 = idE23.matcher(letra);
				if(gE23.find())
				{
					if(word.substring(pula-1,pula)=="#" & word.substring(pula+1,pula+2)=="#")
					{
						letter2[pula] = "i";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idE24 = Pattern.compile("e");
				Matcher gE24 = idE24.matcher(letra);
				if(gE24.find())
				{	
					if(word.substring(pula+1,pula+2)=="#"|word.substring(pula+1,pula+2)=="-")
					{
						letter2[pula] = "i";
						pula = pula + 1;pula2 = pula2 + 1;
					}
					else
					{	
						letter2[pula] = "e";
						pula = pula + 1;pula2 = pula2 + 1;
					}
					desvio = "sair";
				}
			}
				if (desvio!="sair"){
				Pattern idF = Pattern.compile("f");
				Matcher gF = idF.matcher(letra);
				if(gF.find())
				{	
					letter2[pula] = "f";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idG1 = Pattern.compile("g");
				Matcher gG1 = idG1.matcher(letra);
				if(gG1.find())
				{	
					Pattern idG11 = Pattern.compile("[ie]");
					Matcher gG11 = idG11.matcher(word.substring(pula+1,pula+2));
					if(gG11.find())
					{
						letter2[pula] = "Z";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idG2 = Pattern.compile("g");
				Matcher gG2 = idG2.matcher(letra);
				if(gG2.find())
				{	
					Pattern idG21 = Pattern.compile("u[ieêéí]");
					Matcher gG21 = idG21.matcher(word.substring(pula+1,pula+3));
					if(gG21.find())
					{
						letter2[pula] = "g";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idG3 = Pattern.compile("g");
				Matcher gG3 = idG3.matcher(letra);
				if(gG3.find())
				{	
					letter2[pula] = "g";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idH = Pattern.compile("h");
				Matcher gH = idH.matcher(letra);
				if(gH.find())
				{	
					letter2[pula] = "";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				if (strVw == "u")
				{
					if (word.substring(pula-1,pula)=="u")
					{
						Pattern idI1 = Pattern.compile("i");
						Matcher gI1 = idI1.matcher(letra);
						if(gI1.find())
						{
							Pattern idI11 = Pattern.compile("t");
							Matcher gI11 = idI11.matcher(word.substring(pula+1,pula+2));
							if(gI11.find())
							{	
								if (word2 != "#gratuito#"&word2 != "#gratuita#")
								{
									letter2[pula] = "j~";
									pula = pula + 1;pula2 = pula2 + 1;
									desvio = "sair";
								}
								else
								{
									letter2[pula] = "j";
									pula = pula + 1;pula2 = pula2 + 1;
									desvio = "sair";
								}
							}
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idI2 = Pattern.compile("i");
				Matcher gI2 = idI2.matcher(letra);
				if(gI2.find())
				{	
					Pattern idI21 = Pattern.compile("(e#|e-)");
					Matcher gI21 = idI21.matcher(word.substring(pula+1,pula+3));
					if(gI21.find())
					{	
						letter2[pula] = "i";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idI3 = Pattern.compile("[ií]");
				Matcher gI3 = idI3.matcher(letra);
				if(gI3.find())
				{	
					Pattern idI31 = Pattern.compile("[mn]([b-d|f-g|j-n|p-t|v-z]|[#-])");
					Matcher gI31 = idI31.matcher(word.substring(pula+1,pula+3));
					if(gI31.find())
					{	
						letter2[pula] = "i~";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idI4 = Pattern.compile("i");
				Matcher gI4 = idI4.matcher(letra);
				if(gI4.find())
				{	
					Pattern idI41 = Pattern.compile("[mn][haeiouáéíóúãôâõê]");
					Matcher gI41 = idI41.matcher(word.substring(pula+1,pula+3));
					if(gI41.find())
					{	
						letter2[pula] = "i~";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idI5 = Pattern.compile("i");
				Matcher gI5 = idI5.matcher(letra);
				if(gI5.find())
				{	
					Pattern idI51 = Pattern.compile("[aeou]");
					Matcher gI51 = idI51.matcher(word.substring(pula-1,pula));
					if(gI51.find())
					{	
						letter2[pula] = "j";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idI6 = Pattern.compile("[i|í]");
				Matcher gI6 = idI6.matcher(letra);
				if(gI6.find())
				{	
					letter2[pula] = "i";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idJ = Pattern.compile("j");
				Matcher gJ = idJ.matcher(letra);
				if(gJ.find())
				{	
					letter2[pula] = "Z";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idK = Pattern.compile("k");
				Matcher gK = idK.matcher(letra);
				if(gK.find())
				{	
					letter2[pula] = "k";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idL1 = Pattern.compile("l");
				Matcher gL1 = idL1.matcher(letra);
				if(gL1.find())
				{	
					Pattern idL11 = Pattern.compile("[aeiouáéíóúãôâõê]");
					Matcher gL11 = idL11.matcher(word.substring(pula+1,pula+2));
					if(gL11.find())
					{	
						letter2[pula] = "l";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!= "sair"){
				Pattern idL2 = Pattern.compile("l");
				Matcher gL2 = idL2.matcher(letra);
				if(gL2.find())
				{	
					Pattern idL21 = Pattern.compile("h");
					Matcher gL21 = idL21.matcher(word.substring(pula+1,pula+2));
					if(gL21.find())
					{	
						letter2[pula] = "L";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				    else
				    {	
					    letter2[pula] = "w";
					    pula = pula + 1;pula2 = pula2 + 1;
					    desvio = "sair";
				    }  
			        }
				}
				// Nelson 18/03/2010 adiciona a regra L3 e comentada as linhas acima
				/* Como no nosso dialeto e na maioria dos dialetos do português o / l / na
				posição de coda silábica ou seja CVC, na margem da sílaba é
				pronunciado como [ u ], se o núcleo da sílaba for um [ u ] também como
				é o caso das duas palavras, o resultado é dois [u]s juntos, e o
				português faz a elisão de sons semelhantes sempre, principalmente se
				estiverem na mesma sílaba. 					
				Pattern idL3 = Pattern.compile("l");
				Matcher gL3 = idL3.matcher(letra);
				if(gL3.find())
				{	
					Regex idL31 = Pattern.compile("[uú]");
					Matcher gL31 = idL31.matcher(word.substring(pula-1,1));
					if(gL31.find())
					{	
						letter2[pula-1] = "u:";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				  else
				  {	
					  letter2[pula] = "w";
					  pula = pula + 1;pula2 = pula2 + 1;
					  desvio = "sair";
				  }  
			        }*/
				if (desvio!="sair"){
				Pattern idM1 = Pattern.compile("m");
				Matcher gM1 = idM1.matcher(letra);
				if(gM1.find())
				{	
					letter2[pula] = "m";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idN1 = Pattern.compile("n");
				Matcher gN1 = idN1.matcher(letra);
				if(gN1.find())
				{	
					Pattern idN11 = Pattern.compile("h");
					Matcher gN11 = idN11.matcher(word.substring(pula+1,pula+2));
					if(gN11.find())
					{	
						letter2[pula] = "J";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idN2 = Pattern.compile("n");
				Matcher gN2 = idN2.matcher(letra);
				if(gN2.find())
				{	
					letter2[pula] = "n";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (strVw == "o" && desvio!= "sair")
				{	
					Pattern idO1 = Pattern.compile("o");
					Matcher gO1 = idO1.matcher(letra);
					if(gO1.find())
					{	
						Pattern idO11 = Pattern.compile("l([^aeiouáéíóúãôâõêh]|[#-])");
						Matcher gO11 = idO11.matcher(word.substring(pula+1,pula+2));
						if(gO11.find())
						{	
							letter2[pula] = "O";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						} 						
					}
				}
				else
				{
					Pattern idO2 = Pattern.compile("o");
					Matcher gO2 = idO2.matcher(letra);
					if(gO2.find())
					{	
						Pattern idO21 = Pattern.compile("l([^aeiouáéíóúãôâõêh]|[#-])");
						Matcher gO21 = idO21.matcher(word.substring(pula+1,pula+3));
						if(gO21.find())
						{	
							letter2[pula] = "o";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						} 
					}
				}
				if (desvio!="sair"){
				Pattern idO3 = Pattern.compile("o");
				Matcher gO3 = idO3.matcher(letra);
				if(gO3.find())
				{	
					Pattern idO31 = Pattern.compile("u");
					Matcher gO31 = idO31.matcher(word.substring(pula+1,pula+2));
					if(gO31.find())
					{	
						letter2[pula] = "o w";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					} 
				}
				}
				if (strVw == "o" && desvio!="sair")
				{
					Pattern idO4 = Pattern.compile("o");
					Matcher gO4 = idO4.matcher(letra);
					if(gO4.find())
					{	
						Pattern idO41 = Pattern.compile("a[a-z#-]");
						Matcher gO41 = idO41.matcher(word.substring(pula+1,pula+3));
						if(gO41.find())
						{	
							letter2[pula] = "o";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						} 						
					}
				}
				if (desvio!="sair"){
				Pattern idO5 = Pattern.compile("o");
				Matcher gO5 = idO5.matcher(letra);
				if(gO5.find())
				{	
					Pattern idO51 = Pattern.compile("so[-#a-z]");
					Matcher gO51 = idO51.matcher(word.substring(pula+1,pula+4));
					if(gO51.find())
					{	
						letter2[pula] = "o";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					} 
				}
				}
				if (desvio!="sair"){
				Pattern idO6 = Pattern.compile("o");
				Matcher gO6 = idO6.matcher(letra);
				if(gO6.find())
				{	
					Pattern idO61 = Pattern.compile("sa[-#a-z]");
					Matcher gO61 = idO61.matcher(word.substring(pula+1,pula+4));
					if(gO61.find())
					{	
						letter2[pula] = "O";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					} 
				}
				}
				if (desvio!="sair"){
				Pattern idO7 = Pattern.compile("[oô]");
				Matcher gO7 = idO7.matcher(letra);
				if(gO7.find())
				{	
					Pattern idO71 = Pattern.compile("[mn]([^aeiouáéíóúãôâõêh]|#)");
					Matcher gO71 = idO71.matcher(word.substring(pula+1,pula+3));
					if(gO71.find())
					{	
						letter2[pula] = "o~";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					} 
				}
				}
				if (strVw == "o" && desvio!="sair")
				{
					Pattern idO8 = Pattern.compile("o");
					Matcher gO8 = idO8.matcher(letra);
					if(gO8.find())
					{	
						Pattern idO81 = Pattern.compile("[mn]");
						Matcher gO81 = idO81.matcher(word.substring(pula+1,pula+2));
						if(gO81.find())
						{	
							letter2[pula] = "o~";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						} 
					}
				}
				if (desvio!="sair"){
				Pattern idO9 = Pattern.compile("o");
				Matcher gO9 = idO9.matcher(letra);
				if(gO9.find())
				{	
					Pattern idO91 = Pattern.compile("r###");
					Matcher gO91 = idO91.matcher(word.substring(pula+1,pula+5));
					if(gO91.find())
					{	
						for(int o=0;o<exceptions_o.length;o++)
						{
							if (word2 == exceptions_o[o])
							{
								letter2[pula] = "O";
								break;
							}
							else
							{
								letter2[pula] = "o";
							}
						}
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					} 
					else
					{	
						Pattern idO911 = Pattern.compile("res#");
						Matcher gO911 = idO911.matcher(word.substring(pula+1,pula+5));
						if(gO911.find())
						{	
							for(int o=0;o<exceptions_o.length;o++)
							{
								if (word2 == exceptions_o[o])
								{
									letter2[pula] = "O";
									break;
								}
								else
								{
									letter2[pula] = "o";
								}
							}
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						} 					
					}
				}
				}
				if (desvio!="sair"){
				Pattern idO10 = Pattern.compile("o");
				Matcher gO10 = idO10.matcher(word);
				if(gO10.find())
				{	
					Pattern idO101 = Pattern.compile("z#");
					Matcher gO101 = idO101.matcher(word.substring(pula+1,pula+3));
					if(gO101.find())
					{
						if (word2 == "#arroz#")
						{
							letter2[pula] = "o";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
						else
						{
							letter2[pula] = "O";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idOo11 = Pattern.compile("ô");
				Matcher gOo11 = idOo11.matcher(letra);
				if(gOo11.find())
				{	
					letter2[pula] = "o";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idO12 = Pattern.compile("ó");
				Matcher gO12 = idO12.matcher(letra);
				if(gO12.find())
				{	
					letter2[pula] = "O";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idO13 = Pattern.compile("õ");
				Matcher gO13 = idO13.matcher(letra);
				if(gO13.find())
				{	
					letter2[pula] = "o~";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idO14 = Pattern.compile("o");
				Matcher gO14 = idO14.matcher(letra);
				if(gO14.find())
				{	
					if (word.substring(pula-1,pula) == "c" & (word.substring(pula+1,pula+2)== "-" || word.substring(pula+1,pula+2)== "#"))
					{
						if (word.substring(pula-2,pula-1)== "#")	
						{
							letter2[pula] = "o";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				/* Nelson 18/03/2010
				A regra só funcionava para o grafema [a] antes do [o].
				O problema está nos ditongos 'au', 'io', 'ão' cuja última vogal tem a
				mesma pronúncia nos três, portanto a transcrição diferente desta
				última vogal não se justifica, deveria se [w] em todos. */
				if (desvio!="sair"){
				Pattern idO15 = Pattern.compile("o");
				Matcher gO15 = idO15.matcher(letra);
				if(gO15.find())
				{	
					if (word.substring(pula-1,pula)=="a") // || word.substring(pula-1,1)=="i" || word.substring(pula-1,1)=="í")
					{
						Pattern idO151 = Pattern.compile("[^aeiouáéíóúãôâõê]#|##");
						Matcher gO151 = idO151.matcher(word.substring(pula+1,pula+3));
						if(gO151.find())
						{
							letter2[pula] = "w";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idO16 = Pattern.compile("o");
				Matcher gO16 = idO16.matcher(letra);
				if(gO16.find())
				{	
					Pattern idO161 = Pattern.compile ("[a-z]strVw");
					Matcher gO161 = idO161.matcher(word.substring(pula+1,pula+3));
					if(gO161.find())
					{
						letter2[pula] = "o";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idO17 = Pattern.compile("o");
				Matcher gO17 = idO17.matcher(letra);
				if(gO17.find())
				{	
					if (word.substring(pula+1,pula+2) == "#"|word.substring(pula+1,pula+2) == "-")
					{
						letter2[pula] = "u";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idO18 = Pattern.compile("o");
				Matcher gO18 = idO18.matcher(letra);
				if(gO18.find())
				{	
					
					Pattern idO181 = Pattern.compile("(s#|s-)");
					Matcher gO181 = idO181.matcher(word.substring(pula+1,pula+3));
					if(gO181.find())
					{
						letter2[pula] = "u";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idO19 = Pattern.compile("o");
				Matcher gO19 = idO19.matcher(letra);
				if(gO19.find())
				{	
					letter2[pula] = "o";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idP1 = Pattern.compile("p");
				Matcher gP1 = idP1.matcher(letra);
				if(gP1.find())
				{
					if (word.substring(pula+1,pula+2) == "h")
					{
						letter2[pula] = "f";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
					else
					{
			//	}
			//	Pattern idP2 = Pattern.compile("p");
			//	Match gP2 = idP2.matcherer(letra);
			//	if(gP2.find())
	//			{	
					letter2[pula] = "p";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				}
				if (desvio!="sair"){
				Pattern idQ1 = Pattern.compile("q");
				Matcher gQ1 = idQ1.matcher(letra);
				if(gQ1.find())
				{	
					Pattern idQ11 = Pattern.compile("u[ieoíéó]");
					Matcher gQ11 = idQ11.matcher(word.substring(pula+1,pula+3));
					if(gQ11.find())
					{
						letter2[pula] = "k";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idQ2 = Pattern.compile("q");
				Matcher gQ2 = idQ2.matcher(letra);
				if(gQ2.find())
				{	
					letter2[pula] = "k";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idR1 = Pattern.compile("r");
				Matcher gR1 = idR1.matcher(letra);
				if(gR1.find())
				{	
					if(word.substring(pula+1,pula+2) == "r")
					{
						letter2[pula] = "R";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idR3 = Pattern.compile("r");
				Matcher gR3 = idR3.matcher(letra);
				if(gR3.find())
				{	
					if(word.substring(pula-1,pula) == "#")
					{
						letter2[pula] = "R";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idR4 = Pattern.compile("r");
				Matcher gR4 = idR4.matcher(letra);
				if(gR4.find())
				{	
					Pattern idR41 = Pattern.compile("[aeiouáéíóúãôâõê]");
					Matcher gR41 = idR41.matcher(word.substring(pula+1,pula+2));
					if(gR41.find())
					{
						letter2[pula] = "r";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idR8 = Pattern.compile("r");
				Matcher gR8 = idR8.matcher(letra);
				if(gR8.find())
				{	
					Pattern idR81 = Pattern.compile("[ptfsc]");
					Matcher gR81 = idR81.matcher(word.substring(pula+1,pula+2));
					if(gR81.find()|word.substring(pula+1,pula+3)=="ch"| word.substring(pula+1,pula+3)=="sh")
					{
						letter2[pula] = "X";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idR9 = Pattern.compile("r");
				Matcher gR9 = idR9.matcher(letra);
				if(gR9.find())
				{	
					Pattern idR91 = Pattern.compile("[bgdvzjnmr]");
					Matcher gR91 = idR91.matcher(word.substring(pula+1,pula+2));
					if(gR91.find())
					{
						letter2[pula] = "R";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
					else
					{
						Pattern idR92 = Pattern.compile("nh|g[ei]|-r");
						Matcher gR92 = idR92.matcher(word.substring(pula+1,pula+3));
						if(gR92.find()|word.substring(pula+1,pula+4)=="gue"|word.substring(pula+1,pula+4)=="gui")
						{
							letter2[pula] = "R";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idR10 = Pattern.compile("r");
				Matcher gR10 = idR10.matcher(letra);
				if(gR10.find())
				{	
					letter2[pula] = "X";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idS1 = Pattern.compile("s");
				Matcher gS1 = idS1.matcher(letra);
				if(gS1.find())
				{			
					if (word.substring(pula-1,pula) == "n")
					{
						if (word.substring(pula-2,pula-1) == "a"| word.substring(pula-2,pula-1) == "â")
						{
							if (word.substring(pula-3,pula-2) == "r")
							{
								if (word.substring(pula-4,pula-3) == "t")
								{		
									Pattern idS11 = Pattern.compile("[aeiouáéíóúãôâõê]");
									Matcher gS11 = idS11.matcher(word.substring(pula+1,pula+2));
									if(gS11.find())
									{	
										letter2[pula] = "z";
										pula = pula + 1;pula2 = pula2 + 1;
										desvio = "sair";
									}
								}
							}
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idS2 = Pattern.compile("s");
				Matcher gS2 = idS2.matcher(letra);
				if(gS2.find())
				{
					if(word == "#obséquio#")
					{
						letter2[3] = "z";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idS3 = Pattern.compile("s");
				Matcher gS3 = idS3.matcher(letra);
				if(gS3.find())
				{	
					if (word.substring(pula-1,pula) == "ã" & word.substring(pula+1,pula+2) == "#")
					{	
						letter2[pula] = "j~ s";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}	
				}
				}
				if (desvio!= "sair"){
				Pattern idS4 = Pattern.compile("s");
				Matcher gS4 = idS4.matcher(letra);
				if(gS4.find())
				{	
					if (pos != 0)
					{
						if (pula-1 ==pos & word.substring(pula+1,pula+2) == "#")
						{
							if(word.substring(pula-2,pula-1)!="#")
							{	
								letter2[pula] = "j s";
								pula = pula + 1;pula2 = pula2 + 1;
								desvio = "sair";
							}
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idS5 = Pattern.compile("s");
				Matcher gS5 = idS5.matcher(letra);
				if(gS5.find())
				{	
					if (word.substring(pula+1,pula+2) == "h")
					{	
						letter2[pula] = "S";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idS6 = Pattern.compile("s");
				Matcher gS6 = idS6.matcher(letra);
				if(gS6.find())
				{	
					if (word.substring(pula-1,pula) == "#")
					{	
						letter2[pula] = "s";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idS7 = Pattern.compile("s");
				Matcher gS7 = idS7.matcher(letra);
				if(gS7.find())
				{	
					Pattern idS71 = Pattern.compile("[aeiouáéíóúãôâõê]");
					Matcher gS71 = idS71.matcher(word.substring(pula-1,pula));
					if(gS71.find())
					{	
						Pattern idS72 = Pattern.compile("[aeiouáéíóúãôâõê]");
						Matcher gS72 = idS72.matcher(word.substring(pula+1,pula+2));
						if(gS72.find())
						{	
							letter2[pula] = "z";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idS8 = Pattern.compile("s");
				Matcher gS8 = idS8.matcher(letra);
				if(gS8.find())
				{	
					Pattern idS81 = Pattern.compile("[bgdvzjnm]");
					Matcher gS81 = idS81.matcher(word.substring(pula+1,pula+2));
					if(gS81.find())
					{
						letter2[pula] = "z";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";				
					}
					else
					{
						Pattern idS82 = Pattern.compile("rr|nh|g[ei]|-r|r[bdvzgjnmh]");
						Matcher gS82 = idS82.matcher(word.substring(pula+1,pula+3));
						if(gS82.find()|word.substring(pula+1,pula+4)=="gue"|word.substring(pula+1,pula+4)=="gui")
						{
							letter2[pula] = "z";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idS9 = Pattern.compile("s");
				Matcher gS9 = idS9.matcher(letra);
				if(gS9.find())
				{	
					if (word.substring(pula+1,pula+2) == "s")
					{
						letter2[pula] = "s";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idS10 = Pattern.compile("s");
				Matcher gS10 = idS10.matcher(letra);
				if(gS10.find())
				{	
					Pattern idS101 = Pattern.compile("c[ei]");
					Matcher gS101 = idS101.matcher(word.substring(pula+1,pula+3));
					if(gS101.find())
					{
						letter2[pula] = "s";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idSs11 = Pattern.compile("s");
				Matcher gSs11 = idSs11.matcher(letra);
				if(gSs11.find())
				{	
					if (word.substring(pula+1,pula+2) == "ç")
					{
						letter2[pula] = "s";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idS14 = Pattern.compile("s");
				Matcher gS14 = idS14.matcher(letra);
				if(gS14.find())
				{	
					letter2[pula] = "s";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idT1 = Pattern.compile("t");
				Matcher gT1 = idT1.matcher(letra);
				if(gT1.find())
				{	
					Pattern idT11 = Pattern.compile("h#");
					Matcher gT11 = idT11.matcher(word.substring(pula+1,pula+3));
					if(gT11.find())
					{	
						letter2[pula] = "tS";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idT2 = Pattern.compile("t");
				Matcher gT2 = idT2.matcher(letra);
				if(gT2.find())
				{	
					Pattern idT21 = Pattern.compile("h");
					Matcher gT21 = idT21.matcher(word.substring(pula+1,pula+2));
					if(gT21.find())
					{	
						letter2[pula] = "t";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idT3 = Pattern.compile("t");
				Matcher gT3 = idT3.matcher(letra);
				if(gT3.find())
				{
					Pattern idT31 = Pattern.compile("[^aeiouáéíóúãôâõêrl]");
					Matcher gT31 = idT31.matcher(word.substring(pula+1,pula+2));
					if(gT31.find())
					{
						letter2[pula] = "tS";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idT4 = Pattern.compile("t");
				Matcher gT4 = idT4.matcher(letra);
				if(gT4.find())
				{	
					if(word.substring(pula+1,pula+2) == "í")
					{
						letter2[pula] = "tS";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
					else
					{
						if(word.substring(pula+1,pula+2) == "i")
						{
							letter2[pula] = "tS";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
						else
						{
							if (word.substring(pula+1,pula+4) == "es#")
							{
								letter2[pula] = "tS";
								pula = pula + 1;pula2 = pula2 + 1;
								desvio = "sair";
							}
							else
							{
								if (word.substring(pula+1,pula+3) == "e#")
								{
									letter2[pula] = "tS";
									pula = pula + 1;pula2 = pula2 + 1;
									desvio = "sair";
								}	
							}
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idT5 = Pattern.compile("t");
				Matcher gT5 = idT5.matcher(letra);
				if(gT5.find())
				{	
					if(word.substring(pula+1,pula+2) == "#")
					{
						letter2[pula] = "tS";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idT6 = Pattern.compile("t");
				Matcher gT6 = idT6.matcher(letra);
				if(gT6.find())
				{	
					letter2[pula] = "t";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idU1 = Pattern.compile("ü");
				Matcher gU1 = idU1.matcher(letra);
				if(gU1.find())
				{	
					letter2[pula] = "w";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idU2 = Pattern.compile("u");
				Matcher gU2 = idU2.matcher(letra);
				if(gU2.find())
				{	
					Pattern idU21 = Pattern.compile("[mn][^haeiouáéíóúãôâõê]");
					Matcher gU21 = idU21.matcher(word.substring(pula+1,pula+3));
					if(gU21.find())
					{
						letter2[pula] = "u~";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idU3 = Pattern.compile("u");
				Matcher gU3 = idU3.matcher(letra);
				if(gU3.find())
				{	
					Pattern idU31 = Pattern.compile("[mn][-#]");
					Matcher gU31 = idU31.matcher(word.substring(pula+1,pula+3));
					if(gU31.find())
					{
						letter2[pula] = "u~";
						pula = pula + 2;pula2 = pula2 + 2;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idU4 = Pattern.compile("u");
				Matcher gU4 = idU4.matcher(letra);
				if(gU4.find())
				{	
					Pattern idU41 = Pattern.compile("[mn][haeiouáéíóúãôâõê]");
					Matcher gU41 = idU41.matcher(word.substring(pula+1,pula+3));
					if(gU41.find())
					{
						letter2[pula] = "u~";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idU5 = Pattern.compile("[u]");
				Matcher gU5 = idU5.matcher(letra);
				if(gU5.find())
				{	
					Pattern idU51 = Pattern.compile("[gq]");
					Matcher gU51 = idU51.matcher(word.substring(pula-1,pula));
					if(gU51.find())
					{
						Pattern idU52 = Pattern.compile("[aáãâ]");
						Matcher gU52 = idU52.matcher(word.substring(pula+1,pula+2));
						if(gU52.find())
						{
							letter2[pula] = "w";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idU6 = Pattern.compile("[u]");
				Matcher gU6 = idU6.matcher(letra);
				if(gU6.find())
				{	
					Pattern idU61 = Pattern.compile("[g]");
					Matcher gU61 = idU61.matcher(word.substring(pula-1,pula));
					if(gU61.find())
					{
						Pattern idU62 = Pattern.compile("[oóôõ]");
						Matcher gU62 = idU62.matcher(word.substring(pula+1,pula+2));
						if(gU62.find())
						{
							letter2[pula] = "w";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idU7 = Pattern.compile("u");
				Matcher gU7 = idU7.matcher(letra);
				if(gU7.find())
				{	
					Pattern idU71 = Pattern.compile("[aeiouáéíóúãôâõê]");
					Matcher gU71 = idU71.matcher(word.substring(pula-1,pula));
					if(gU71.find())
					{
						letter2[pula] = "w";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
//					Pattern idU8 = Pattern.compile("u");
//					Matcher gU8 = idU8.matcher(letra);
//					if(gU8.find())
//					{	
//						Regex idU81 = Pattern.compile("l");
//						Matcher gU81 = idU81.matcher(word.substring(pula+1,1));
//						if(gU81.find())
//						{
//							letter2[pula] = "u:";
//							pula = pula + 2;pula2 = pula2 + 2;
//							desvio = "sair";
//						}
//					}
				if (desvio!="sair"){
				Pattern idU8 = Pattern.compile("[uú]");
				Matcher gU8 = idU8.matcher(letra);
				if(gU8.find())
				{	
					letter2[pula] = "u";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idV1 = Pattern.compile("v");
				Matcher gV1 = idV1.matcher(letra);
				if(gV1.find())
				{	
					letter2[pula] = "v";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idW1 = Pattern.compile("w");
				Matcher gW1 = idW1.matcher(letra);
				if(gW1.find())
				{	
					letter2[pula] = "w";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}//<i><x> = LIXO
				}
				if (desvio!="sair"){
				Pattern idX1 = Pattern.compile("x");
				Matcher gX1 = idX1.matcher(letra);
				if(gX1.find())
				{
					if(word.substring(pula-1,pula) == "i")
					{	
						if (word.substring(pula-2,pula-1) != "f"&word.substring(pula-2,pula-1) != "m")	
						{
							letter2[pula] = "S";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
						else
						{
							letter2[pula] = "k s";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				// <(W_bgn)(e,ê)><x><V,C_v>...
				if (desvio!="sair"){
				Pattern idX3 = Pattern.compile("x");
				Matcher gX3 = idX3.matcher(letra);
				if(gX3.find())
				{
					Pattern idX31 = Pattern.compile("[aeiouáéíóúãôâõêbgdvzjnm]");
					Matcher gX31 = idX31.matcher(word.substring(pula+1,pula+2));
					if(gX31.find()|word.substring(pula+1,pula+4)=="gue"|word.substring(pula+1,pula+4)=="gui"||word.substring(pula+1,pula+3)=="rr"|word.substring(pula+1,pula+3)=="nh"|word.substring(pula+1,pula+4)=="ge"|word.substring(pula+1,pula+3)=="gi"|word.substring(pula+1,pula+3)=="-r")
					{	
						if (word.substring(pula-1,pula)=="e"|word.substring(pula-1,pula)=="ê")
						{
							if (word.substring(pula-2,pula-1)=="#")
							{
								letter2[pula] = "z";
								pula = pula + 1;pula2 = pula2 + 1;
								desvio = "sair";
							}
							else
							{	
								if (word.substring(pula-2,pula-1)=="h")
								{
									if (word.substring(pula-3,pula-2)=="#")
									{
										letter2[pula] = "z";
										pula = pula + 1;pula2 = pula2 + 1;
										desvio = "sair";
									}
								}
							}
						}
					}
				}
				}// <(W_bgn)(ine)><x><o,C_v>b,d,g,v,z,Z,r,R,n,m,J,h //("nh|g[ei]|#r");bgdvzjnmr
						//Matcher gR92 = idR92.matcher(word.substring(pula+1,2));
						//if(gR92.find()|word.substring(pula+1,3)=="gue"|word.substring(pula+1,3)=="gui")
				if (desvio!="sair"){
				Pattern idX4 = Pattern.compile("x");
				Matcher gX4 = idX4.matcher(letra);
				if(gX4.find())
				{
					Pattern idX41 = Pattern.compile("[oóõôbgdvzjnm]");
					Matcher gX41 = idX41.matcher(word.substring(pula+1,pula+2));
					if(gX41.find()|word.substring(pula+1,pula+4)=="gue"|word.substring(pula+1,pula+4)=="gui")
					{	
						if (word.substring(pula-1,pula)=="e")
						{
							if (word.substring(pula-2,pula-1)=="n")
							{
								if (word.substring(pula-3,pula-2)=="i")
								{
									if (word.substring(pula-4,pula-3)=="#")
									{
										letter2[pula] = "k s";
										pula = pula + 1;pula2 = pula2 + 1;
										desvio = "sair";
									}
								}
							}
						}
					}
					else
					{
						Pattern idX42 = Pattern.compile("(rr|nh|g[ei]|-r|r[bdvzgjnmh])");
						Matcher gX42 = idX42.matcher(word.substring(pula+1,pula+3));
						if(gX42.find())
						{	
							if (word.substring(pula-1,pula)=="e")
							{
								if (word.substring(pula-2,pula-1)=="n")
								{
									if (word.substring(pula-3,pula-2)=="i")
									{
										if (word.substring(pula-4,pula-3)=="#")
										{
											letter2[pula] = "k s";
											pula = pula + 1;pula2 = pula2 + 1;
											desvio = "sair";
										}
									}
								}
							}	
						}
					}
				}
				}
				// <(W_bgn)(ine)><x><a,e,i>
				if (desvio!="sair"){
				Pattern idX5 = Pattern.compile("x");
				Matcher gX5 = idX5.matcher(letra);
				if(gX5.find())
				{
					Pattern idX51 = Pattern.compile("[aeiáéíâêã]");
					Matcher gX51 = idX51.matcher(word.substring(pula+1,pula+2));
					if(gX51.find())
					{	
						if (word.substring(pula-1,pula)=="e")
						{
							if (word.substring(pula-2,pula-1)=="n")
							{
								if (word.substring(pula-3,pula-2)=="i")
								{
									if (word.substring(pula-4,pula-3)=="#")
									{
										letter2[pula] = "z";
										pula = pula + 1;pula2 = pula2 + 1;
										desvio = "sair";
									}
								}
							}
						}
					}
					else
					{
						Pattern idX52 = Pattern.compile("(rr|nh|g[ei]|-r|r[bdvzgjnmh])");
						Matcher gX52 = idX52.matcher(word.substring(pula+1,pula+3));
						if(gX52.find()|word.substring(pula+1,pula+4)=="gue"|word.substring(pula+1,pula+4)=="gui")
						{	
							if (word.substring(pula-1,pula)=="e")
							{
								if (word.substring(pula-2,pula-1)=="n")
								{
									if (word.substring(pula-3,pula-2)=="i")
									{
										if (word.substring(pula-4,pula-3)=="#")
										{
											letter2[pula] = "z";
											pula = pula + 1;pula2 = pula2 + 1;
											desvio = "sair";
										}
									}
								}
							}	
						}
					}
				}
				}//<(W_bgn)(e,ê,ine)><x><V,C_uv>p,t,k,f,s,S  som de 's'
				if (desvio!="sair"){
				Pattern idX6 = Pattern.compile("x");
				Matcher gX6 = idX6.matcher(letra);
				if(gX6.find())
				{	
					Pattern idX61 = Pattern.compile("[ptfsc]");
					Matcher gX61 = idX61.matcher(word.substring(pula+1,pula+2));
					if(gX61.find()|word.substring(pula+1,pula+3)=="ch"| word.substring(pula+1,pula+3)=="sh")
					{	
						if (word.substring(pula-1,pula)=="ê"||word.substring(pula-1,pula)=="e")
						{
							if (word.substring(pula-2,pula-1)=="#")
							{
								letter2[pula] = "s";
								pula = pula + 1;pula2 = pula2 + 1;
								desvio = "sair";
							}
							else
							{
								if (word.substring(pula-1,pula)=="e")
								{
									if (word.substring(pula-2,pula-1)=="n")
									{
										if (word.substring(pula-3,pula-2)=="i")
										{
											if (word.substring(pula-4,pula-3)=="#")
											{
												letter2[pula] = "s";
												pula = pula + 1;pula2 = pula2 + 1;
												desvio = "sair";
											}
										}
									}
								}
							}
						}
					}
				}
				}//<(W_bgn)( e )><x><Hf><V,C_v>...    som de 'z'
				if (desvio!="sair"){
				Pattern idX7 = Pattern.compile("x");
				Matcher gX7 = idX7.matcher(letra);
				if(gX7.find())
				{	
					Pattern idX71 = Pattern.compile("[-][aeiouáéíóúãôâõêbgdvzjnmh]");
					Matcher gX71 = idX71.matcher(form_grafema.substring(pula2+1,pula2+3));
					if(gX71.find())
					{	
						if (word.substring(pula-1,pula)=="e")
						{
							if (word.substring(pula-2,pula-1)=="#")
							{
								letter2[pula] = "z";
								pula = pula + 1;pula2 = pula2 + 1;
								desvio = "sair";
							}
						}
					}
					else
					{
						Pattern idX72 = Pattern.compile("[-](rr|nh|g[ei]|-r|r[bdvzgjnmh])");
						Matcher gX72 = idX72.matcher(form_grafema.substring(pula2+1,pula2+4));
						if(gX72.find()|form_grafema.substring(pula2+1,pula2+4)=="gue"|form_grafema.substring(pula2+1,pula2+4)=="gui")
						{
							if (word.substring(pula-2,pula-1)=="#e")
							{
								letter2[pula] = "z";
								pula = pula + 1;pula2 = pula2 + 1;
								desvio = "sair";
							}
						}
					}
				}
				}//...<(W_bgn)( e )><x><Hf><C_uv>som de 's'
				if (desvio!="sair"){
				Pattern idX8 = Pattern.compile("x");
				Matcher gX8 = idX8.matcher(letra);
				if(gX8.find())
				{	
					Pattern idX81 = Pattern.compile("[-][ptkfs]");
					Matcher gX81 = idX81.matcher(form_grafema.substring(pula2+1,pula2+3));
					if(gX81.find()|form_grafema.substring(pula2+1,pula2+4)=="-ch"| form_grafema.substring(pula2+1,pula2+4)=="-sh")
					{	
						if (word.substring(pula-2,pula-1)=="#e")
						{
							letter2[pula] = "s";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idX9 = Pattern.compile("x");	
				Matcher gX9 = idX9.matcher(letra);
				if(gX9.find())
				{
					if(form_grafema.substring(pula2+1,1) == "-")
					{	
						letter2[pula] = "k z";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				//...<V - e><x><V>...    som de 'k s'     
				if (desvio!="sair"){
				Pattern idX10 = Pattern.compile("x");
				Matcher gX10 = idX10.matcher(letra);
				if(gX10.find())
				{	
					Pattern idX101 = Pattern.compile("[aiouáíóúãôâõ]");
					Matcher gX101 = idX101.matcher(word.substring(pula-1,1));
					if(gX101.find())
					{
						Pattern idX102 = Pattern.compile("[aeiouáéíóúãôâõê]");
						Matcher gX102 = idX102.matcher(word.substring(pula+1,1));
						if(gX102.find())
						{
							for(int t=0;t<exceptions.length;t++)
							{
								if (word2 == exceptions[t])
								{
									letter2[pula] = "k s";
									break;
								}
								else
								{
									letter2[pula] = "S";aux[pula] = "x10";
								}
							}
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				//x & 10 & ...(b,f,m,p,v) (e) (x) (V)... & [S] & axial \\
				if (desvio!="sair"){
				Pattern idX11 = Pattern.compile("x");
				Matcher gX11 = idX11.matcher(letra);
				if(gX11.find())
				{
					if (word.substring(pula-1,1)=="e")
					{
						Pattern idX111 = Pattern.compile("[bfmpv]");
						Matcher gX111 = idX111.matcher(word.substring(pula-2,1));
						if(gX111.find())
						{
							Pattern idX112 = Pattern.compile("[aeiouáéíóúãôâõê]");
							Matcher gX112 = idX112.matcher(word.substring(pula+1,1));
							if(gX112.find())
							{
								if (word2 == "#convexo#")
								{	
									letter2[pula] = "k s";
									pula = pula + 1;pula2 = pula2 + 1;
									desvio = "sair";
								}
								else
								{
									letter2[pula] = "S";
									pula = pula + 1;pula2 = pula2 + 1;
									desvio = "sair";
								}
							}
						}
					}
				}
				}
				//x & 11 & ...(V) (e) (x) (V)... & [z] & axial \\
				if (desvio!="sair"){
				Pattern idX12 = Pattern.compile("x");
				Matcher gX12 = idX12.matcher(letra);
				if(gX12.find())
				{
					if (word.substring(pula-1,1)=="e")
					{
						Pattern idX121 = Pattern.compile("[aeiouáéíóúãôâõê]");
						Matcher gX121 = idX121.matcher(word.substring(pula-2,1));
						if(gX121.find())
						{
							Pattern idX122 = Pattern.compile("[aeiouáéíóúãôâõê]");
							Matcher gX122 = idX122.matcher(word.substring(pula+1,1));
							if(gX122.find())
							{	
								letter2[pula] = "z";
								pula = pula + 1;pula2 = pula2 + 1;
								desvio = "sair";
							}
						}
					}
				}
				}
				//x & 12 & ...(C-b,f,m,p,v) (e) (x) (V)... & [k s] & axial \\
				if (desvio!="sair"){
				Pattern idX13 = Pattern.compile("x");
				Matcher gX13 = idX13.matcher(letra);
				if(gX13.find())
				{
					if (word.substring(pula-1,1)=="e")
					{
						Pattern idX131 = Pattern.compile("[^bfmpvaeiouáéíóúãôâõê]");
						Matcher gX131 = idX131.matcher(word.substring(pula-2,1));
						if(gX131.find())
						{
							Pattern idX132 = Pattern.compile("[aeiouáéíóúãôâõê]");
							Matcher gX132 = idX132.matcher(word.substring(pula+1,1));
							if(gX132.find())
							{	
								letter2[pula] = "k s";
								pula = pula + 1;pula2 = pula2 + 1;
								desvio = "sair";
							}
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idX14 = Pattern.compile("x");
				Matcher gX14 = idX14.matcher(letra);
				if(gX14.find())
				{
					if (word.substring(pula-1,1)=="#")
					{
						letter2[pula] = "S";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idX15 = Pattern.compile("x");
				Matcher gX15 = idX15.matcher(letra);
				if(gX15.find())
				{
					if (word.substring(pula-1,1)=="e"|word.substring(pula-1,1)=="é"|word.substring(pula-1,1)=="ê")
					{
						Pattern idX151 = Pattern.compile("[^aeiouáéíóúãôâõê#]");
						Matcher gX151 = idX151.matcher(word.substring(pula+1,1));
						if(gX151.find())
						{
							letter2[pula] = "s";
							pula = pula + 1;pula2 = pula2 + 1;
							desvio = "sair";
						}
					}
				}
				}
				if (desvio!="sair"){
				Pattern idX16 = Pattern.compile("x");
				Matcher gX16 = idX16.matcher(letra);
				if(gX16.find())
				{
					if (word.substring(pula+1,1)=="#")
					{
						letter2[pula] = "k s";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
					else
					{
						letter2[pula] = "S";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idY1 = Pattern.compile("y");
				Matcher gY1 = idY1.matcher(letra);
				if(gY1.find())
				{	
					Pattern idY11 = Pattern.compile("[^aeiouáéíóúãôâõê]");
					Matcher gY11 = idY11.matcher(word.substring(pula+1,1));
					if(gY11.find())
					{
						letter2[pula] = "i";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idY2 = Pattern.compile("y");
				Matcher gY2 = idY2.matcher(letra);
				if(gY2.find())
				{	
					letter2[pula] = "j";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				}
				if (desvio!="sair"){
				Pattern idZ1 = Pattern.compile("z");
				Matcher gZ1 = idZ1.matcher(letra);
				if(gZ1.find())
				{	
					if (word.substring(pula-1,1) == strVw & word.substring(pula+1,1) == "#")
					{
						letter2[pula] = "j s";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idZ2 = Pattern.compile("z");
				Matcher gZ2 = idZ2.matcher(letra);
				if(gZ2.find())
				{	
					if (word.substring(pula+1,1) == "#")
					{
						letter2[pula] = "s";
						pula = pula + 1;pula2 = pula2 + 1;
						desvio = "sair";
					}
				}
				}
				if (desvio!="sair"){
				Pattern idZ3 = Pattern.compile("z");
				Matcher gZ3 = idZ3.matcher(letra);
				if(gZ3.find())
				{	
					letter2[pula] = "z";
					pula = pula + 1;pula2 = pula2 + 1;
					desvio = "sair";
				}
				
			}
				}
			}
			}while (desvio == "sair");
			}while (volta=="volta");
			//salvar:

			svwl.write("A vogal tonica da palavra {0} é {1} na posição {2}."+grafema+strVw+ind_svw+"\n");
				grafema = grafema + "    ";
			for (int i=1;i< word2.length();i++) 
			{
				palavra = palavra + letter2[i] + " ";
			}
			transc = grafema + palavra;
			tw.write(transc+"\n");	
			word = reader.readLine();
			word2 = reader2.readLine();
			grafema = format.readLine();
			contador++; 
				}
		reader.close();
		reader2.close();
		format.close();
		tw.close();
		svwl.close();
	}
}
			if (count==20){
				desvio = "sair";
			}
			count = count+1;
		}
		if (n==20){
			desvio = "sair";
		}
		n = n+1;
		}  
	} 
 public static void main(String[] args) throws IOException{
		//string dicionario = args[0];
		//string sp = args[1];			
		FileWriter conv = new FileWriter("./convert.dic");
		FileWriter str = new FileWriter("./stress.dic");
		BufferedReader java_grammar = new BufferedReader(new FileReader("./file.grammar"));
		String dicionario = "file.grammar";
		String strr;
		String con;
		boolean fone = false;
		String format;
		format = java_grammar.readLine();
		
		while (format!=null)
		{
			if (format == "<s>" || format == "</s>")
			{
				format = java_grammar.readLine();
			}
			else
			{
				strr = ""; con = "";
				String [] compword = format.split("-");
				for /*each*/ (String parte: compword)
				 	{	
					strr = strr + "#"+parte+"#";
					con = con + "#"+parte+"######";
				}
				//Console.WriteLine(format);
				str.write(strr+"\n");
				conv.write(con+"\n");
				format = java_grammar.readLine();
			}
		}	
				
		/*if (sp == "true")
		{
			fone = true;
		}*/
		fone = true;
		str.close();
		conv.close();
		java_grammar.close();
		ConversorFonetico id = new ConversorFonetico();
		id.detStress(fone,dicionario);
		/*if(conv.exists()))
		{
			try
			{
				System.IO.File.Delete(@"./.convert.dic");
			}
			catch (System.IO.IOException e)
			{
				//Console.WriteLine(e.Message);
				return;
			}
		}
		if(System.IO.File.Exists(@"./.stress.dic"))
		{
			try
			{
				System.IO.File.Delete(@"./.stress.dic");
			}
			catch (System.IO.IOException e)
			{
				//Console.WriteLine(e.Message);
				return;
			}
		}*/
	}
}
