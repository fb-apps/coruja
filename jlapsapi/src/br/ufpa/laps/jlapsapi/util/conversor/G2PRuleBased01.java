package br.ufpa.laps.jlapsapi.util.conversor;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import components.stress.StressComponent;
//import components.syll.SyllabificationRuleBased01;

/**
 * An grapheme to phoneme converter. The algorithm implemented here is an
 * adaptation of paper:
 * <p>
 * 
 * Denilson C. Silva, Amaro A. de Lima, R. Maia, Daniela Braga, 
 * Joao F. de Moraes, Joao A. de Moraes and Fernando G. V. Resende Jr.  
 * "A rule-based grapheme-phone converter and stress determination
 * for Brazilian Portuguese natural language processing." 
 * in Proceedings of IEEE International Telecommunications Symposium 
 * ITS 2006.
 * <p>
 * 
 * Last modification in May 28, 2010
 * @author Igor Couto
 * 
**/


public class G2PRuleBased01 extends G2PComponent{

	private String palavra="";
	private String saida="";
	private String[] palavraCompleta;
	private String separadorLetras=" ";
	//private static boolean silabacao=false;
	private SyllabificationRuleBased01 sil;
	
	/* Exceptions for rule of < x > grapheme  k s */
	private static String[] x_exceptions_ks = {"oxítono","oxítona","oxítonas",
		"oxítonos","oxidar","anexar", "oxigênio","oxiúro","oxalato","úter","uxoricida",
		"axila","axiologia","íxia","táxis","táxi","sintaxe"};
	/* Exceptions for rule of < x > grapheme  k z */
	private static String[] x_exceptions_kz = {"ixofagia","ixomielite","ixolite","ixômetro",
		"ixora","ixoscopia","ox-acético"};
	
	public G2PRuleBased01(StressComponent stress) {
		super(stress);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unused")
	private int[] executaSilabacao(){

		int[] indSilabas;
		int nsilabas;
		int tSilaba=0;
		sil = new SyllabificationRuleBased01();
		String pSeparada = sil.process(palavra);
		StringTokenizer st = new StringTokenizer(pSeparada,"-");
		nsilabas=st.countTokens();
		indSilabas=new int[nsilabas];
		for (int i = 0; i < nsilabas; i++){
			tSilaba=st.nextToken().length();
			
				indSilabas[i]=tSilaba;
			
		}
		return indSilabas;
	}
	
	public String converteParte(String entrada, int inicio, int fim) {
		
		//int indice = 0;
		/* Converte para caixa-baixa. */
		palavra=entrada.toLowerCase();
		
		saida="";
//		/* Temporárias s*/
//		int inicio=1;
//		int fim=entrada.length();
//		int[] indSil;
//		/* Converter selarando silabicamente? */
//		if(silabacao){
//			indSil=executaSilabacao();
//			
//			
//		}
//				
		/* Evita analisar Strings vazias ou com caracteres inválidos*/
		if(entrada.equals("")){
			saida="Entrada vazia.";
			return saida;
		}
		//Pattern p = Pattern.compile("(([^abcdefghijklmnopqrstuvwxyzáéíóúàâêôãõü-]))");
		Pattern p = Pattern.compile("(([^a-zçáéíóúàâêôãõü-]))");
		Matcher m = p.matcher(entrada);
		if(m.find()){
			saida="Entrada com caracteres inválidos:  "+m.group();
			return saida;
		}
		/* Capta o tamanho da palavra. */
		int tamanho=entrada.length();
		/* Trata para evitar índices inválidos. */
		if(inicio < 1 || inicio > tamanho){
			saida="Índice de início inválido: "+inicio;
			return saida;
		}
		if(fim > tamanho || fim < 1 || fim < inicio){
			saida="Índice de fim inválido: "+fim;
			return saida;
		}
		/* Subtrai 1 de todos os índices, pois em vetores sempre começam em 0.*/
		//tamanho--;
		inicio--;
		fim--;
		
		/* Variável para armazenar partes das palavras com hifen.
		 * Quando a palavra não tem hífem, palavraCompleta.length=0 */
		palavraCompleta = separaHifen(palavra);
		/* Palavra com hífen? */
		boolean hifen=false;
		if(palavraCompleta.length > 1)
			hifen=true;
		/* Armazenará o índice de bogal tônica. */
		int stressInd;
		
		String ppalavra="";
		
		for (int parte = 0; parte < palavraCompleta.length; parte++) {
			/* Armazena a parte da palavraCompleta em palavra. */
			palavra=palavraCompleta[parte];
			tamanho = palavra.length()-1;
						
			/* Somente para palavras com hífen. */
			if(palavraCompleta.length==2 && parte==0)
				ppalavra = palavraCompleta[parte+1];
			if(palavraCompleta.length==3 && parte==1)
				ppalavra = palavraCompleta[parte+2];
			
			//inicio=0;
		//	fim=tamanho;
			/* Converterá somente a parte de interesse. */			
			for (int ind = inicio; ind <= fim; ind++) {
								
				/* Inicializa todos as variáveis de apoio. */
				char aaaaletra='#',aaaletra='#',aaletra='#',aletra='#',
				letra='#',pletra='#',ppletra='#',pppletra='#',ppppletra='#';
				/* Inicializa as variáveis que armazenarão as letras à esquerda,
				 * evitando ArrayOutOfBoundsExceptions. */
				switch (ind) {
					default: /* Em casos que o [ind] é maior que 3. */
					case 4:aaaaletra=palavra.charAt(ind-4);
					case 3: aaaletra=palavra.charAt(ind-3);
					case 2:  aaletra=palavra.charAt(ind-2);
					case 1:	  aletra=palavra.charAt(ind-1);
					case 0:    letra=palavra.charAt(ind);
						break;
				}
				/* Inicializa as variáveis que armazenarão as letras à direita, evitando ArrayOutOfBoundsExceptions. */
				switch (tamanho-ind) {
					default: /* Em casos que o [fim-ind] é maior que 3. */
					case 4:ppppletra=palavra.charAt(ind+4);	
					case 3: pppletra=palavra.charAt(ind+3);
					case 2:  ppletra=palavra.charAt(ind+2);
					case 1:	  pletra=palavra.charAt(ind+1);
					case 0:	
						break;
				}	
				/* Transforma o char em um objeto Enum para utilziar a estrutura switch. */
				Alphabet grafema = Alphabet.valueOf(""+letra);
				switch (grafema) {
					/*===================================================================*/
					/* Rule 01 - Grapheme sequence for < a > algorithm. */
					case a:
						/* Sub rule 01-01  ... < an > < Pont > ...  <-->  [a~]   Iv.an. Itapo.an.*/
						if( pletra == 'n' && ppletra == '#' ){
							saida=saida+"a~"+separadorLetras;
							//System.out.print("a~  ");
							ind++;
							break;
						}
						/* Sub rule 01-02  ... < am > < Pont > ...  <-->  [a~w~]   and.am. cresç.am.*/
						if( pletra == 'm' && ppletra == '#' ){
							saida=saida+"a~ w~"+separadorLetras;
							//System.out.print("a~ w~  ");
							ind++;
							break;
						}
						/* Sub rule 01-03  ... < a(V_ton) > < m,n > < V,h > ...  <-->  [a~]   c.a.ma  b.a.nho */
						stressInd=stress.process(palavra);
						if( (stressInd == ind +1)
								&&(pletra=='m'||pletra=='n')
								&&(isVowel(ppletra)||ppletra=='h')){
							saida=saida+"a~"+separadorLetras;
							//System.out.print("a~ ");
							break;
						}
						/* Sub rule 01-04  ... < â(m,n) > < C - h >...  <-->  [a~]   l.â.mpada  c.â.ntico */
						// IN NEXT CASE STATEMENT
						/* Sub rule 01-05  ... < ão > ...  <-->  [a~w~]  avi.ão. */
						// IN NEXT CASE STATEMENT
						/* Sub rule 01-06  ... < ã,â > ...  <-->  [a~]  c.â.mara amanh.ã. */
						// IN NEXT CASE STATEMENT
						/* Sub rule 01-07  ... < á,à > ...  <-->  [a]  Ant.á.rtica  .à.quela */
						// IN NEXT CASE STATEMENT
						/* Sub rule 01-08  ... < a(m,n) > < C - h >...  <-->  [a~]  .an.tropofagia  .am.perímetro  */
						if( (pletra=='m' || pletra=='n') && (!isVowel(ppletra) && ppletra != 'h') ){
							saida=saida+"a~"+separadorLetras;
							//System.out.print("a~  ");
							ind++;
							break;
						}
						/* Sub rule 01-09  ... < a > ...  <-->  [a]  .a.r.a.cnofobia */
							saida=saida+"a"+separadorLetras;
							//System.out.print("a ");
					break;
						case â:
						/* Sub rule 01-04  ... < â (m,n) > < C - h >...  <-->  [a~]   l.âmpada  c.ân.tico */
						if( (pletra=='m'||pletra=='n') && (!isVowel(ppletra)&&ppletra!='h') ){
							saida=saida+"a~"+separadorLetras;
							//System.out.print("a~  ");
							ind++;
							break;
						}
						/* Sub rule 01-06  ... < ã,â > ...  <-->  [a~]  c.â.mara */
							saida=saida+"a~"+separadorLetras;
							//System.out.print("a~ ");
						break;
						case ã:
						/* Sub rule 01-05  ... < ão > ...  <-->  [a~w~]  avi.ão. */
						if(pletra=='o'){
							saida=saida+"a~ w~"+separadorLetras;
							//System.out.print("a~ w~  ");
							ind++;
							break;
						}
						/* Sub rule 01-06  ... < ã,â > ...  <-->  [a~]  amanh.ã. */
						saida=saida+"a~"+separadorLetras;
						//System.out.print("a~ ");
						break;
						case á:
						/* Sub rule 01-07  ... < á,à > ...  <-->  [a]  Ant.á.rtica */
						saida=saida+"a"+separadorLetras;
						//System.out.print("a ");	
						break;
						case à:
						/* Sub rule 01-07  ... < á,à > ...  <-->  [a]  .à.quela */
						saida=saida+"a"+separadorLetras;
						//System.out.print("a ");	
						break;
				/*===================================================================*/
				/* Rule 02 - Grapheme sequence for <b> algorithm. */
				case b:
					/* Sub rule 02-01  <b>  <-->  [b] */
					saida=saida+"b"+separadorLetras;
					//System.out.print("b ");
				break;
				/*===================================================================*/
				/* Rule 03 - Grapheme sequence for < c > algorithm. */
				case c:
					/* Sub rule 03-01  ... < c > < e,i > ...  <-->  [s]   a.c.eitar ja.c.into */
					if(pletra=='e'||pletra=='é'||pletra=='ê'||pletra=='i'||pletra=='í'){
						saida=saida+"s"+separadorLetras;
						break;
					}
					/* Sub rule 03-02  ... < ç > ...  <-->  [s]   almo.ç.o */
					// IN NEXT CASE STATEMENT
					
					/* Sub rule 03-03   ... < c h > ...  <-->  [S]   a.ch.o    */
					if(pletra=='h'){
						saida=saida+"S"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 03-04  ... < c > ...  <-->  [k]  .c.laro     */
					saida=saida+"k"+separadorLetras;
					break;
					/* Sub rule 03-02  ... < ç > ...  <-->  [s]   almo.ç.o */
					case ç:
						saida=saida+"s"+separadorLetras;
					break;				
				/*===================================================================*/
				/* Rule 04 - Grapheme sequence for < d > algorithm. */
				case d:
					/* Sub rule 04-01  ... < d > < i,[i] > ...  <-->  [dZ]   .d.ia  tar.d.e */
					if( pletra == 'i' || pletra=='y'
						||(pletra=='e'&& ppletra=='#')
						||(pletra=='e'&&ppletra=='s'&&pppletra=='#')){
						saida=saida+"dZ"+separadorLetras;
						break;
					}
					/* Sub rule 04-02  ... < d > < C-(r,l)> ...  <-->  [dZ]   a.d.vogado  */
					if( !isVowel(pletra) && (pletra!='r'&&pletra!='l') ){
						saida=saida+"dZ"+separadorLetras;
						break;
					}
					/* Sub rule 04-03   ... < d > < Pont >...  <-->  [dZ]   Rai.d.    */
					if( pletra == '#' ){
						saida=saida+"dZ"+separadorLetras;
						break;
					}
					/* Sub rule 04-04  ... < d > ...  <-->  [d]  .d.ote .d.raga     */
						saida=saida+"d"+separadorLetras;
				break;
				/*===================================================================*/
				/* Rule 05 - Grapheme sequence for < e > algorithm. */
				case e:
					/* Sub rule 05-01  ... < en > < Pont > ...  <-->  [e~]  híf.en. Carm.en. */
					if( (pletra == 'n' && ppletra == '#') || ( pletra == 'n' && ppletra == 's' && pppletra == '#')){
						saida=saida+"e~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 05-02  ... < e(V_ton) > < l > < C-h,Pont > ...  <-->  [E]  pap.e.l  m.e.l */
					stressInd=stress.process(palavra);
					if( stressInd == (ind+1) && pletra == 'l' && ( (!isVowel(ppletra) && ppletra!='h' ) || ppletra=='#' )){
						saida=saida+"E"+separadorLetras;
						break;
					}
					/* Sub rule 05-03  ... < e(V_ton) > < l > < C-h,Pont > ...  <-->  [e]  sensív.e.l  s.e.lvagem */
					stressInd=stress.process(palavra);
					if( stressInd != (ind+1) && pletra == 'l' && ( (!isVowel(ppletra) && ppletra!='h' ) || ppletra=='#' )){
						saida=saida+"e"+separadorLetras;
						break;
					}
					/* Sub rule 05-04  ... < õ,ã > < e > ...  <-->  [j~]  mã.e.s   coraçõ.e.s */
					if( aletra == 'ã' || aletra == 'õ' ){
						saida=saida+"j~"+separadorLetras;
						break;
					}
					/* Sub rule 05-05  ... < a > < e > ...  <-->  [j]  Ca.e.tano */
					if( aletra == 'a'){
						saida=saida+"j"+separadorLetras;
						break;
					}
					/* Sub rule 05-06  ... < (Prn_M) > < e > < V_ton > ...  <-->  [e]  .e.le .e.ste */
					stressInd=stress.process(palavra);
					if( (stressInd == ind+1) 
							&& hasString(Prn_M, entrada) ){
						saida=saida+"e"+separadorLetras;
						break;
					}
					/* Sub rule 05-07  ... < (Prn_F) > < e(V_ton) > ...  <-->  [E]  .e.la .e.sta */
					stressInd=stress.process(palavra);
					if( (stressInd == ind+1) 
							&& hasString(Prn_F, entrada) ){
						saida=saida+"E"+separadorLetras;
						break;
					}
					/* Sub rule 05-08  ... < (W_bgn)e > < x > < V > ...  <-->  [e]   .e.xato  .e.xiste */
					if( aletra == '#' && pletra=='x' && isVowel(ppletra) ){
						saida=saida+"e"+separadorLetras;
						break;
					}
					/* Sub rule 05-09  ... < (W_bgn)e > < x,s > < V > ...  <-->  [e]   .e.xcelente .e.scola */
					if( aletra == '#' && (pletra=='x' || pletra=='s') && !isVowel(ppletra) ){
						saida=saida+"e"+separadorLetras;
						break;
					}
					/* Sub rule 05-10  ... < (W_bgn)e > < C-(m,n,x)  > ...  <-->  [e]   .e.rrado .e.lefante */
					if( aletra == '#' && (!isVowel(pletra) && pletra!='m' && pletra!='n' && pletra!='x' ) ){
						saida=saida+"e"+separadorLetras;
						break;
					}
					/* Sub rule 05-11  ... < e > < ne,me > < Pont > ...  <-->  [e]  cr.e.me  sol.e.ne */
					if( ((pletra == 'n' && ppletra == 'e') || (pletra == 'm' && ppletra == 'e')) && pppletra == '#' ){
						saida=saida+"e"+separadorLetras;
						break;
					}
					/* Sub rule 05-12  ... < e > < la,lo > < Pont > ...  <-->  [E]  v.e.la p.e.lo  */
					if( ((pletra == 'l' && ppletra == 'a') || (pletra == 'l' && ppletra == 'o')) && pppletra == '#' ){
						if ( entrada.equals("pelo")||entrada.equals("pela")){
							saida=saida+"e"+separadorLetras;
						}else{
							saida=saida+"E"+separadorLetras;
						}
							
								
						break;
					}
					/* Sub rule 05-13  ... < e > < sa,ssa,za > < Pont > ...  <-->  [e]  chin.e.sa cond.e.ssa riqu.e.za */
					if( ((pletra=='s'&&ppletra=='a') || (pletra=='z'&&ppletra=='a') || (pletra=='s'&&ppletra=='s'&&pppletra=='a') ) && (pppletra=='#'||ppppletra=='#') ){
						saida=saida+"e"+separadorLetras;
						break;
					}
					/* Sub rule 05-14  ... < ê (m,n) > < C-h > ...  <-->  [e~]  ci.ên.cia  exist.ên.cia */
					// IN NEXT CASE STATEMENT
					/* Sub rule 05-15  ... < e (m,n) > < C-h > ...  <-->  [e~]  .em.bora .en.tonação */
					if( ( pletra=='m'||pletra=='n' ) && ( !isVowel(ppletra) && ppletra!='h') && pppletra!='#' ){
						saida=saida+"e~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 05-16  ... < e(V_ton) > < m,n > ...  <-->  [e~]  t.e.ma  p.e.na */
					stressInd=stress.process(palavra);
					if(stressInd==(ind+1)
							&&(pletra=='m'||pletra=='n')
							&&(ppletra!='#')){
						saida=saida+"e~"+separadorLetras;
						break;
					}
					/* Sub rule 05-17  ... < (e,é,ê)m > < Pont > ...  <-->  [e~j~]  cont.ém.  t.êm. ont.em. */
					/* THIS RULES IS DIVIDED IN TREE PARTS. THE OTHER IS IN NEXT CASE STATEMENTS */
					if( pletra == 'm' && ppletra == '#' ){
						saida=saida+"e~ j~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 05-18   ... < ê > ...  <-->  [e]   beb.ê.    */
					// IN NEXT CASE STATEMENT
					/* Sub rule 05-19   ... < é > ...  <-->  [E]   .é.poca    */
					// IN NEXT CASE STATEMENT
					/* Sub rule 05-20  ... < e > < s > < Pont > ...  <-->  [i]   fras.e.s  */
					if( pletra=='s' && ppletra=='#' ){
						saida=saida+"i"+separadorLetras;
						break;
					}
					/* Sub rule 05-21  ... < e(V_ton)i > ...  <-->  [ej]  fiqu.ei.  ar.ei.a  */
					stressInd=stress.process(palavra);
					if( stressInd == (ind+1) && pletra == 'i' ){
						saida=saida+"e j"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 05-22  ... < e > < Ltr > < V_ton > ...  <-->  [e]  s.e.mestre  p.e.rigo */
					stressInd=stress.process(palavra);
					if( isLetter(pletra) && stressInd == (ind+2))  {
						saida=saida+"e"+separadorLetras;
						break;
					}
					/* Sub rule 05-23  ... < Pont > < e > < Pont > ...  <-->  [i]  João .e. a Ana   */
					if( aletra=='#' && pletra=='#' ){
						saida=saida+"i"+separadorLetras;
						break;
					}
					/* Sub rule 05-24  ... < e > < Pont > ...  <-->  [i]     índic.e.   */
					if( pletra=='#' ){
						saida=saida+"i"+separadorLetras;
						break;
					}
					/* Sub rule 05-25  ... < e > ...  <-->  [e]     b.e.bê   */
						saida=saida+"e"+separadorLetras;
					
				break;
					case ê:
					/* Sub rule 05-14  ... < ê (m,n) > < C-h > ...  <-->  [e~]  ci.ên.cia  exist.ên.cia */
					if( ( pletra=='m'||pletra=='n' ) && ( !isVowel(ppletra) && ppletra!='h') && pppletra!='#' ){
						saida=saida+"e~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 05-17  ... < (e,é,ê)m > < Pont > ...  <-->  [e~j~]  cont.ém.  t.êm. ont.em. */
					if( pletra == 'm' && ppletra == '#' ){
						saida=saida+"e~ j~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 05-18   ... < ê > ...  <-->  [e]   beb.ê.    */
						saida=saida+"e"+separadorLetras;
					break;
					case é:
					/* Sub rule 05-17  ... < (e,é,ê)m > < Pont > ...  <-->  [e~j~]  cont.ém.  t.êm. ont.em. */
					if( pletra == 'm' && ppletra == '#' ){
						saida=saida+"e~ j~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 05-19   ... < é > ...  <-->  [E]   .é.poca    */
						saida=saida+"E"+separadorLetras;
					break;
				/*===================================================================*/
				/* Rule 06 - Grapheme sequence for <f> algorithm. */
				case f:
					/* Sub rule 06-01  <f>  <-->  [f] */
					saida=saida+"f"+separadorLetras;
				break;
				/*===================================================================*/
				/* Rule 07 - Grapheme sequence for < g > algorithm. */
				case g:
					/* Sub rule 07-01  ... < g > < e,i > ...  <-->  [Z]   .g.elo .g.irassol */
					if( pletra == 'e' || pletra == 'i' ){
						saida=saida+"Z"+separadorLetras;
						break;
					}											
					/* Sub rule 07-02   ... < g u > < e,i > ...  <-->  [g]   .gu.indaste .gu.eixa   */
					if( pletra == 'u' && (ppletra == 'e' || ppletra == 'ê'  || ppletra == 'é' || ppletra == 'i' || ppletra == 'í' )){
						saida=saida+"g"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 07-03  ... < g > ...  <-->  [g]  .g.aroto     */
						saida=saida+"g"+separadorLetras;
				break;
				/*===================================================================*/
				/* Rule 08 - Grapheme sequence for < h > algorithm. */
				case h:
					/* Sub rule 08-01 ... < h > ... <-->  [*]   .h.oje */
					saida=saida+"";					
				break;
				/*===================================================================*/
				/* Rule 09 - Grapheme sequence for < i > algorithm. */
				case i:
					/* Sub rule 09-01 ... < u (V_ton)> < i > < t > ... <-->  [j~]   mu.i.to */
					stressInd=stress.process(palavra);
					if( aletra == 'u' && stressInd == (ind) && pletra=='t'){
						saida=saida+"j~"+separadorLetras;
						break;
					}
					/* Sub rule 09-02 ... < i e > < Pont > ... <-->  [i]   superfíc.ie. */
					if( pletra == 'e' && ppletra=='#' ){
						saida=saida+"i"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 09-03 ... < i(m,n) > < C-(h,Pont) > ... <-->  [i~]  
					 *  s.ím.bolo  f.im.  s.in.to  ac.im.ba  */
					/* THIS RULES IS DIVIDED IN TWO PARTS. THE OTHER IS IN NEXT CASE STATEMENT */
					if((pletra=='m'||pletra == 'n')  
							&&((!isVowel(ppletra)&& (ppletra!='h'&&ppletra!='#')))){
						saida=saida+"i~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 09-04 ... < i > < m,n > < V,h > ... <-->  [i~]   .i.noperante  .i.maginar n.i.nho */
					if( (pletra=='m'||pletra=='n') && ( isVowel(ppletra)||ppletra=='h') ) {
						saida=saida+"i~"+separadorLetras;
						break;
					}
					/* Sub rule 09-05 ... < V -i > < i > ... <-->  [j]   co.i.sa sa.i. */
					if( isVowel(aletra) &&  aletra!='i' ){
						saida=saida+"j"+separadorLetras;
						break;
					}
					/* Sub rule 09-06 ... < i > ... <-->  [i]   l.i.qu.i.dação */
						saida=saida+"i"+separadorLetras;
				break;
					case í:
					/* Sub rule 09-03 ... < i (m,n) > < C - (h,Pont) > ... <-->  [i~]  
					 *  s.ím.bolo  f.im.  s.in.to  ac.im.ba  */
					if( (pletra=='m'||pletra=='n')  
							&&(!isVowel(ppletra) 
							&&(ppletra!='h'||ppletra!='#')) ){
						saida=saida+"i~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 09-06 ... < i > ... <-->  [i]   l.í.quido sa.í.  */
						saida=saida+"i"+separadorLetras;
					break;
				/*===================================================================*/
				/* Rule 10 - Grapheme sequence for <j> algorithm. */
				case j:
					/* Sub rule 10-01  <j>  <-->  [Z] */
					saida=saida+"Z"+separadorLetras;
				break;
				/*===================================================================*/
				/* Rule 11 - Grapheme sequence for < k > algorithm. */
				case k:
					/* Sub rule 11-01  <k>  <-->  [k]  .k.elvin */
					saida=saida+"k"+separadorLetras;
				break;
				/*===================================================================*/
				/* Rule 12 - Grapheme sequence for < l > algorithm. */
				case l:
					/* Sub rule 12-01  ... < l > <V > ...  <-->  [l]   a.l.a */
					if( isVowel(pletra) ){
						saida=saida+"l"+separadorLetras;
						break;
					}
					/* Sub rule 12-02   ... < l h > ...  <-->  [L]   a.lh.o    */
					if( pletra == 'h' ){
						saida=saida+"L"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 12-03  ... < l > ...  <-->  [w]  voga.l.     */
					saida=saida+"w"+separadorLetras;
											
				break;
				/*===================================================================*/
				/* Rule 13 - Grapheme sequence for < m > algorithm. */
				case m:
					/* Sub rule 13-01  ... < e,é,ê,i > < m > < sp > <V > ...  <-->  [J] 
					 *   Algué.m. usou  Que.m. está */
					// NOT IMPLEMENTED BECAUSE THIS METHOD ANALYZE A WORD AT A TIME. 
					/* Sub rule 13-02  ... < m > ...  <-->  [m] */
						saida=saida+"m"+separadorLetras;
				break;
				/*===================================================================*/
				/* Rule 14 - Grapheme sequence for < n > algorithm. */
				case n:
					/* Sub rule 14-01 ... < nh >...  <-->  [J] */
					if(pletra == 'h'){
						saida=saida+"J"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 14-02  ... < n > ...  <-->  [n] */
						saida=saida+"n"+separadorLetras;
				break;
					
				/*===================================================================*/
				/* Rule 15 - Grapheme sequence for < o > algorithm. */
				case o:
					/* Sub rule 15-01  ... < o(V_ton) > < l > < C-h,Pont > ...  <-->  [O]  girass.o.l   s.o.l */
					stressInd=stress.process(palavra);
					if( stressInd == (ind+1) && pletra=='l' && ( (!isVowel(ppletra)&&ppletra!='h' )||ppletra=='#' )){
						saida=saida+"O"+separadorLetras;
						break;
					}
					/* Sub rule 15-02  ... < o(V_aton) > < l > < C-h,Pont > ...  <-->  [o]  s.o.ldadura  s.o.ltar */
					stressInd=stress.process(palavra);
					if( stressInd!=(ind+1)&& pletra=='l' && ( (!isVowel(ppletra) && ppletra!='h' ) || ppletra=='#' )){
						saida=saida+"o"+separadorLetras;
						break;
					}
					/* Sub rule 15-03 ... < ou >...  <-->  [ow]   .ou.vir c.ou.ve */
					if(pletra=='u'){
						saida=saida+"ow"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 15-04  ... < o(V_ton) > < a > < Pont,Ltr > ...  <-->  [o]  Lisb.o.a pess.o.a */
					stressInd=stress.process(palavra);
					if( stressInd == (ind+1) && pletra=='a' && ( ppletra=='#' || isLetter(ppletra) )){
						saida=saida+"o"+separadorLetras;
						break;
					}
					/* Sub rule 15-05  ... < o > < so > < Pont,Ltr > ...  <-->  [o]  saud.o.so  virtu.o.so */
					if(  pletra=='s' && ppletra=='o' && ( pppletra	=='#' || isLetter(pppletra) )){
						saida=saida+"o"+separadorLetras;
						break;
					}
					/* Sub rule 15-06  ... < o > < sa > < Pont,Ltr > ...  <-->  [O]  saud.o.sa  virtu.o.sa */
					if(  pletra == 's' && ppletra=='a' && ( pppletra=='#' || isLetter(pppletra) )){
						saida=saida+"O"+separadorLetras;
						break;
					}
					/* Sub rule 15-07  ... < o,ô (m,n) > < C-h,Pont > ...  <-->  [o~]  compositor  g.ô.ndola  som */
					/* THIS RULES IS DIVIDED IN TWO PARTS. THE OTHER IS IN NEXT CASE STATEMENT */
					if( (pletra=='m'||pletra=='n') && ( (!isVowel(ppletra)&&ppletra!='h') || ppletra=='#' )){
						saida=saida+"o~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 15-08  ... < o(V_ton) > < m,n > ...  <-->  [o~]  s.o.ma  ris.o.nho */
					stressInd=stress.process(palavra);
					if( stressInd == (ind+1) && (pletra=='m'||pletra=='n')){
						saida=saida+"o~"+separadorLetras;
						break;
					}
					/* Sub rule 15-09  ... < o > < r > < Pont > ...  <-->  [o]  comp.o.r  d.o.r  am.o.r */
					/* Sub rule 15-09 - exception  */
					if ( hasString(o_exceptions, entrada)){
						saida=saida+"O"+separadorLetras;
						break;
					}
					if(  pletra=='r' && ppletra=='#' ){
						saida=saida+"o"+separadorLetras;
						break;
					}
					/* Sub rule 15-10  ... < o > < z > < Pont > ...  <-->  [O]  v.o.z    alg.o.z   */
					/* Sub rule 15-10 - exception  */
					if ( entrada.equals("arroz")){
						saida=saida+"o"+separadorLetras;
						break;
					}
					if(  pletra=='z' && ppletra=='#' ){
						saida=saida+"O"+separadorLetras;
						break;
					}
					/* Sub rule 15-11   ... < ô > ...  <-->  [o]   vov.ô.    */
					// IN NEXT CASE STATEMENT
					/* Sub rule 15-12   ... < ó > ...  <-->  [O]   vov.ó.    */
					// IN NEXT CASE STATEMENT
					/* Sub rule 15-13   ... < õ > ...  <-->  [o~]   coraç.õ.es    */
					// IN NEXT CASE STATEMENT
					/* Sub rule 15-14  ... < (W_bgn)c > < o > < Pont > ...  <-->  [o]   c.o.-autor */
					if( aaletra == '#' && aletra == 'c' && pletra == '#'){
						saida=saida+"o"+separadorLetras;
						break;
					}
					/* Sub rule 15-15  ... < a > < o > < C,Pont > ...  <-->  [w]   a.o.s  ca.o.s */
					if( aletra=='a' && (pletra=='#'||!isVowel(pletra))){
						saida=saida+"w"+separadorLetras;
						break;
					}
					/* Sub rule 15-16  ... < o > < Ltr > < V_ton > ...  <-->  [o]  .o.por */
					stressInd=stress.process(palavra);
					if( isLetter(pletra) && stressInd == (ind))  {
						saida=saida+"o"+separadorLetras;
						break;
					}
					/* Sub rule 15-17  ... < o > < Pont > ...  <-->  [u]   músico  */
					if( pletra=='#' ){
						saida=saida+"u"+separadorLetras;
						break;
					}
					/* Sub rule 15-18  ... < o > < s > < Pont > ...  <-->  [u]   carros  */
					if( pletra=='s' && ppletra=='#' ){
						saida=saida+"u"+separadorLetras;
						break;
					}					
					/* Sub rule 15-19   ... < o > ...  <-->  [p]   esc.o.po    */
						saida=saida+"o"+separadorLetras;
				break;
					/* Sub rule 15-07  ... < o,ô (m,n) > < C-h,Pont > ...  <-->  [o~]  compositor  g.ô.ndola  som */
					case ô:
					if( (pletra== 'm'||pletra=='n') && ( (!isVowel(ppletra)&&ppletra!='h' ) || ppletra=='#' )){
						saida=saida+"o~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 15-11   ... < ô > ...  <-->  [o]   vov.ô.    */
					saida=saida+"o"+separadorLetras;
					break;
					case ó:
					/* Sub rule 15-12   ... < ó > ...  <-->  [O]   vov.ó.    */
					saida=saida+"O"+separadorLetras;
					break;
					case õ:
					/* Sub rule 15-13   ... < õ > ...  <-->  [o~]   corações    */
					saida=saida+"o~"+separadorLetras;
					break;
				/*===================================================================*/
				/* Rule 16 - Grapheme sequence for < p > algorithm. */
				case p:
					/* Sub rule 16-01  ... < ph > ...  <-->  [f]   .Ph.ilipe */
					if( pletra=='f' ){
						saida=saida+"f"+separadorLetras;
						break;
					}
					/* Sub rule 16-02   ... < p > ...  <-->  [p]   .p.ato    */
						saida=saida+"p"+separadorLetras;
				break;
				/*===================================================================*/
				/* Rule 17 - Grapheme sequence for < q > algorithm. */
				case q:
					/* Sub rule 17-01  ... < qu > < e,i,o > ...  <-->  [k]   .qu.into .qu.eijo .qu.ota */
					if( pletra== 'u' && (ppletra=='e'||ppletra=='i'||ppletra=='o') ){
						saida=saida+"k"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 17-02   ... < q > < ü,u,a > ...  <-->  [k]  cin.q.üenta .q.uase */
					if( pletra=='ü' || (pletra=='u'&&ppletra=='a') ){
						saida=saida+"k"+separadorLetras;
						break;
					}
					saida=saida+"k"+separadorLetras;
				break;					
				/*===================================================================*/
				/* Rule 18 - Grapheme sequence for < r > algorithm. */
				case r:
					/* Sub rule 18-01  ... < rr > ...  <-->  [R]   ca.rr.o */
					if( pletra=='r' ){
						saida=saida+"R"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 18-02  ... < r sp r > ...  <-->  [R]   poma.r r.odeado */
					// NOT IMPLEMENTED BECAUSE THIS METHOD ANALYZE A WORD AT A TIME. 
					/* Sub rule 18-03  ... < BEGIN > < r > ...  <-->  [R]   .r.ua */
					if( aletra=='#' ){
						saida=saida+"R"+separadorLetras;
						break;
					}
					/* Sub rule 18-04  ... < r > < V > ...  <-->  [r]   ratoei.r.a */
					if( isVowel(pletra) ){
						saida=saida+"r"+separadorLetras;
						break;
					}
					/* Sub rule 18-05  ... < r > < sp > < V,h > ...  <-->  [r]   Falta acerta.r. apenas uma. */
					// NOT IMPLEMENTED BECAUSE THIS METHOD ANALYZE A WORD AT A TIME.
					/* Sub rule 18-06  ... < r > < sp > < C_uv > ...  <-->  [X]   Peca.r. pelo meio do caminho */
					// NOT IMPLEMENTED BECAUSE THIS METHOD ANALYZE A WORD AT A TIME.
					/* Sub rule 18-07  ... < r > < sp > < C_v > ...  <-->  [R]   Injeta.r. grãos de arroz */
					// NOT IMPLEMENTED BECAUSE THIS METHOD ANALYZE A WORD AT A TIME.
					/* Sub rule 18-08  ... < r > < C_uv > ...  <-->  [X]   pe.r.co */
					if( isUnvoicedConsonant(pletra) ){
						saida=saida+"X"+separadorLetras;
						break;
					}
					/* Sub rule 18-09  ... < r > < C_v > ...  <-->  [R]   ca.r.ga */
					if( isVoicedConsonant(pletra+ppletra+"")  ){
						saida=saida+"R"+separadorLetras;
						break;
					}
					/* Sub rule 18-10  ... < r > ...  <-->  [X]  Ela irá se lasca.r. */
						saida=saida+"X"+separadorLetras;
					break;
				/*===================================================================*/
				/* Rule 19 - Grapheme sequence for < s > algorithm. */
				case s:
					/* Sub rule 19-01  ... < tr(a,â)n > < s > < V > ...  <-->  [z]   trân.s.ito  tran.s.ação */
					if( aaaaletra == 't' && aaaletra == 'r' &&  
							( aaletra == 'a' || aaletra == 'â' ) && 
							aletra == 'n' && isVowel(pletra) ){
						saida=saida+"z"+separadorLetras;
						break;
					}
					/* Sub rule 19-02  ... < ob > < s > < équio > ...  <-->  [z]   ob.s.équio */
					if( entrada.equals("obséquio") || entrada.equals("obsequio") ){
						saida=saida+"z"+separadorLetras;
						break;
					}
					/* Sub rule 19-03  ... < ã > < s > < Pont > ...  <-->  [j~ s]   fã.s. */
					if( aletra=='ã'&&pletra=='#' ){
						saida=saida+"j~ s"+separadorLetras;
						break;
					}
					/* Sub rule 19-04  ... < V_ton > < s > < Pont > ...  <-->  [j s]   ma.s. atrá.s.  */
					stressInd=stress.process(palavra);
					if( stressInd==(ind) && pletra == '#' ){
						saida=saida+"j s"+separadorLetras;
						break;
					}
					/* Sub rule 19-05  ... < sh > ...  <-->  [S]   .sh.iatsu  */
					if( pletra == 'h' ){
						saida=saida+"S"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 19-06  ... < (W_bgn)s > ...  <-->  [s]   sapato  */
					if( aletra == '#' ){
						saida=saida+"s"+separadorLetras;
						break;
					}
					/* Sub rule 19-07  ... < V > < s > < V >...  <-->  [z]   a.s.a  */
					if( isVowel(aletra) && isVowel(pletra)  ){
						saida=saida+"z"+separadorLetras;
						break;
					}
					/* Sub rule 19-08  ... < s > < C_v >...  <-->  [z]   tran.s.gredir  */
					if( isVoicedConsonant(pletra+ppletra+"")  ){
						saida=saida+"z"+separadorLetras;
						break;
					}
					/* Sub rule 19-09  ... < ss > ...  <-->  [s]   a.ss.ar */
					if( pletra == 's' ){
						saida=saida+"s"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 19-10  ... < sc > < e,i > ...  <-->  [s]   cre.sc.er */
					if( pletra == 'c' && ( ppletra=='e' || ppletra=='i' ) ){
						saida=saida+"s"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 19-11  ... < sç > ...  <-->  [s]   cre.sç.am */
					if( pletra == 'c' && ( ppletra=='e' || ppletra=='i' ) ){
						saida=saida+"s"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 19-12  ... < s sp j > ...  <-->  [Z]   Ele.s. jogam */
					// NOT IMPLEMENTED BECAUSE THIS METHOD ANALYZE A WORD AT A TIME.
					/* Sub rule 19-13  ... < s > < sp > < V,C_uv,h > ...  <-->  [z]   O.s. aros são cromados */
					// NOT IMPLEMENTED BECAUSE THIS METHOD ANALYZE A WORD AT A TIME.
					/* Sub rule 19-14  ... < s > ...  <-->  [s]   Ele.s. receberam o prêmio */
						saida=saida+"s"+separadorLetras;
					break;					
				/*===================================================================*/
				/* Rule 20 - Grapheme sequence for < t > algorithm. */
				case t:
					/* Sub rule 20-01  ... < th > < Pont > ...  <-->  [tS]   Ru.th. */
					if( pletra == 'h' && ppletra == '#' ){
						saida=saida+"tS"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 20-02  ... < th > ...  <-->  [t]   Ar.th.ur */
					if( pletra == 'h' ){
						saida=saida+"t"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 20-03  ... < t > < C - (r,l) >...  <-->  [tS]   algori.t.mo */
					if( !isVowel(pletra) && pletra != 'r'  &&  pletra != 'l' ){
						saida=saida+"tS"+separadorLetras;
						break;
					}
					/* Sub rule 20-04  ... < t > < i,[i]> ...  <-->  [tS]   .t.ia me.t.e */
					if( pletra == 'i'  || pletra == 'í'  || pletra=='y' || (pletra=='e' && ppletra=='#') || (pletra=='e' && ppletra=='s' && pppletra=='#') ){
						saida=saida+"tS"+separadorLetras;
						break;
					}
					/* Sub rule 20-05  ... < t > < Pont > ...  <-->  [tS]   se.t. */
					if( pletra == '#' ){
						saida=saida+"tS"+separadorLetras;
						break;
					}
					/* Sub rule 20-06  ... < t > ...  <-->  [t]   .t.enente .t.a.t.o */
						saida=saida+"t"+separadorLetras;
				break;
				/*===================================================================*/
				/* Rule 21 - Grapheme sequence for < u > algorithm. */
				case u:
					/* Sub rule 03-01  ... < ü > ...  <-->  [w]   ling.ü.istica */
					// IN NEXT CASE STATEMENT
					/* Sub rule 03-02  ... < u (m,n) > < C-h > ...  <-->  [u~]  ab.un.dante  */
					if((pletra== 'm'||pletra=='n')
							&& (!isVowel(ppletra)&&ppletra!='h')){
						saida=saida+"u~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 03-03  ... < u (m,n) > < Pont > ...  <-->  [u~]  at.um.  */
					if((pletra=='m'||pletra== 'n')
							&& pletra=='#'){
						saida=saida+"u~"+separadorLetras;
						ind++;
						break;
					}
					/* Sub rule 03-04  ... < u > < m,n > < V,h > ...  <-->  [u~]  .u.ma .u.nha  */
					if((pletra=='m'||pletra=='n')
							&&(isVowel(ppletra)||ppletra=='h')){
						saida=saida+"u~"+separadorLetras;
						break;
					}
					/* Sub rule 03-05  ... < V-u > < u > ...  <-->  [w]    */
					if(isVowel(aletra)&&aletra!='u'){
						saida=saida+"w"+separadorLetras;
						break;
					}
					/* Sub rule 03-06  ... < q,g > < u > < a > ...  <-->  [w]   ig.u.al  */
					if((aletra=='q'||aletra=='g')
							&&pletra=='a'){
						saida=saida+"w"+separadorLetras;
						break;
					}
					/* Sub rule 03-07  ... < g > < u > < o > ...  <-->  [w]    */
					if((aletra=='q'||aletra=='g')
							&&pletra=='o'){
						saida=saida+"w"+separadorLetras;
						break;
					}
					
					/* Sub rule 03-08  ... < u > ...  <-->  [u]   .u.va */
						saida=saida+"u"+separadorLetras;
				break;
					/* Sub rule 03-01  ... < ü > ...  <-->  [w]   ling.ü.istica */
					case ü:
						saida=saida+"w"+separadorLetras;
					break;
					/* Sub rule 03-05  ... < ú > ...  <-->  [u]   .ú.nico */
					case ú:
						saida=saida+"u"+separadorLetras;
					break;
				/*===================================================================*/
				/* Rule 22 - Grapheme sequence for < v > algorithm. */
				case v:
					/* Sub rule 22-01 ... <v> ... <-->  [v]   .v.oando*/
					saida=saida+"v"+separadorLetras;
				break;
				/*===================================================================*/
				/* Rule 23 - Grapheme sequence for < w > algorithm. */
				case w:
					/* Sub rule 23-01 ... <w> ...  <-->  [w]  .w.att */
					saida=saida+"w"+separadorLetras;
				break;	
				/*===================================================================*/
				/* Rule 24 - Grapheme sequence for < x > algorithm. */
				case x:
					/* Sub rule 24-01 ... < V,C-(f,m) > < i > < x > ...  <-->  [S]      */
					if ((isVowel(aaletra)||(!isVowel(aaletra) && aaletra!='f' && aaletra!='m')) && aletra=='i') {
						if(hasString(x_exceptions_kz, palavra))
							saida=saida+"kz"+separadorLetras;
						else 
							saida=saida+"S"+separadorLetras;	
							
						break;
					}
					/* Sub rule 24-02 ... < (f,m) > < i > < x > ...  <-->  [k s]   fi.x.ar */
					if ((aaletra=='f'||aaletra=='m')&& (aletra=='i')) {
						saida=saida+"ks"+separadorLetras;
						break;
					}
					/* Sub rule 24-03 ... < (W_bgn)e,ê > < x > < V,C_v > ...  <-->  [z]   e.x.iste */
					if ((aaletra=='#'&&(aletra=='e'||aletra=='ê'))&&(isVowel(pletra)||isVoicedConsonant(pletra+ppletra+""))) {
						saida=saida+"z"+separadorLetras;
						break;
					}
					/* Sub rule 24-04 ... < (W_bgn)ine > < x > < o,C_v > ...  <-->  [k s]   ine.x.o */
					if ((aaaletra=='i'&&aaletra=='n'&&aletra=='e')&&(pletra=='o'||isVoicedConsonant(pletra+ppletra+""))) {
						saida=saida+"ks"+separadorLetras;
						break;
					}
					/* Sub rule 24-05 ... < (W_bgn)ine > < x > < a,e,i > ...  <-->  [z]   ine.x.istente */
					if ((aaaletra=='i'&&aaletra=='n'&&aletra=='e')&&(pletra=='a'||pletra=='e'||pletra=='i' )) {
						saida=saida+"z"+separadorLetras;
						break;
					}
					/* Sub rule 24-06 ... < (W_bgn)e,ê,ine > < x > < C_v > ...  <-->  [s]   e.x.posicao */
					if ((aaletra=='#'&&(aletra=='e'||aletra=='ê'||(aaaletra=='i'&&aaletra=='n'&&aletra=='e')))
							&&(isUnvoicedConsonant(pletra))){
						saida=saida+"s"+separadorLetras;
						break;
					}
					if(hifen){
					/* Sub rule 24-07 ... < (W_bgn)e > < x > < Hf > < V,C_v > ...  <-->  [z] e.x.-namorado */ 
					if((parte<palavraCompleta.length-1&&palavraCompleta.length<=2)
							&&aaletra=='#'  
							&&aletra=='e'  
							&&pletra=='#'
							&&(isVowel(ppalavra.charAt(0))||isVoicedConsonant(ppalavra.substring(0, 2))) ){
						saida=saida+"z"+separadorLetras;
						break;
					}
					/* Sub rule 24-08 ... < (W_bgn)e > < x > < Hf > < C_uv > ...  <-->  [s]    */
					if((parte<palavraCompleta.length-1&&palavraCompleta.length<=2)
							&&aaletra=='#'  
							&&aletra=='e'  
							&&pletra=='#'
							&&isUnvoicedConsonant(ppalavra.charAt(0)) ){
						saida=saida+"s"+separadorLetras;
						break;
					}
					/* Sub rule 24-09 ... < V-e > < x > < Hf > < Ltr > ...  <-->  [k z ]    */
					if((parte<palavraCompleta.length-1&&palavraCompleta.length<=2)
							&&(isVowel(aletra)&&aletra!='e')  
							&&aletra=='e'  
							&&pletra=='#'
							&&isLetter(ppalavra.charAt(0)) ){
						saida=saida+"kz"+separadorLetras;
						break;
					}
					}
					/* Sub rule 24-10 ... < V-e > < x > < V > ...  <-->  [k s]  a.x.ila   */
					if ((isVowel(aletra)&&aletra!='e')
							&&(isVowel(pletra))){
						if(hasString(x_exceptions_ks, palavra))
							saida=saida+"ks"+separadorLetras;
						else 
							saida=saida+"S"+separadorLetras;	
							
						break;
					}
					
					
					/* Sub rule 24-11 ... < b,f,m,p,v,x > < e > < x > < V > ...  <-->  [S]   me.x.e  */
					if ((aaletra=='b'||aaletra=='f'||aaletra=='m'||aaletra=='p'||aaletra=='v'||aaletra=='x')
							&&(aletra=='e')
							&&(isVowel(pletra))){
						saida=saida+"S"+separadorLetras;
						break;
					}
					/* Sub rule 24-12 ... < V > < e > < x > < V > ...  <-->  [z]   ie.x.o  */
					if ((isVowel(aaletra))&&(aletra=='e')&&(isVowel(pletra))){
						saida=saida+"z"+separadorLetras;
						break;
					}
					/* Sub rule 24-13 ... < C-(b,f,m,p,v,x) > < e > < x > < V > ...  <-->  [k s]   perple.x.o  */
					if ((!isVowel(aaletra)&&(aaletra!='b'||aaletra!='f'||aaletra!='m'||aaletra!='p'||aaletra!='v'||aaletra!='x'))
							&&(aletra=='e')
							&&(isVowel(pletra))){
						saida=saida+"ks"+separadorLetras;
						break;
					}
											
					/* Sub rule 24-14 ... < (W-bgn)x >  ...  <-->  [S]   .x.exeu  */
					if (aletra=='#'){
						saida=saida+"S"+separadorLetras;
						break;
					}
					/* Sub rule 24-15 ... < e,é,ê > < x > < C >  ...  <-->  [s]   e.x.celente  */
					if ((aletra=='e'||aletra=='é'||aletra=='ê')
							&&(!isVowel(pletra)) && ppletra!='#' ){
						saida=saida+"s"+separadorLetras;
						break;
					}
					/* Sub rule 24-16 ... < x > < Pont >  ...  <-->  [k s]   fa.x.  */
					if (pletra=='#'){
						saida=saida+"ks"+separadorLetras;
						break;
					}
					/* Sub rule 24-17 ... < x > < Pont >  ...  <-->  [S]     */							
						saida=saida+"S"+separadorLetras;
				break;

				/*===================================================================*/
				/* Rule 25 - Grapheme sequence for < y > algorithm. */
				case y:
					/* Sub rule 25-01 ... < y > < C > ... <-->  [i]  .Y.guaçu   */
					if( !isVowel(pletra) ){
						saida=saida+"i"+separadorLetras;
						break;
					}
					/* Sub rule 25-02 ... < y > ...  <-->  [j]  .Y.anomami   */
					if( pletra=='j' ){
						saida=saida+"j"+separadorLetras;
						break;
					}
				break;
				/*===================================================================*/
				/* Rule 26 - Grapheme sequence for < z > algorithm. */
				case z:
					/* Sub rule 26-03 ... < V_ton > < z > < Pont > ... <-->  [j s]  fa.z.   */
					stressInd=stress.process(palavra);
					if(stressInd==(ind)&&pletra=='#'){
						saida=saida+"js"+separadorLetras;
						break;
					}
					/* Sub rule 26-04 ... < z > < Pont > ...  <-->  [s]  O que José fe.z.?   */
					if( pletra=='#' ){
						saida=saida+"s"+separadorLetras;
						break;
					}
					/* Sub rule 26-05 ... < z > ...  <-->  [z]  .z.umbido */
						saida=saida+"z"+separadorLetras;
				break;
								
				default:
					System.err.print("Cannot convert letter "+palavra);
					saida="";
					return saida;
				}
//				
			}
			
		}
		for (int i=0;i<saida.length();i++){
			
		}
		return saida;
	}
	
	@Override
	public String process(String input) {
		// TODO Auto-generated method stub
		/* Variável para armazenar partes das palavras com hifen */
		palavraCompleta = separaHifen(input);
		
		for (int parte = 0; parte < palavraCompleta.length; parte++) {
			/* Armazena a palavra */
			palavra=palavraCompleta[parte];
			int tamanho=palavra.length();
			
			for (int ind = 0; ind < tamanho; ind++) {
				
			}
			
		}
			
		return saida;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private String[] separaHifen(String word){
		
		/* First of all, verifies if the grapheme has hyphen*/
		/* String that will be process */
		String [] splittedWord = new String[1];
		/* Rule of hyphen - Words with hyphen */
		Pattern patt_00 = Pattern.compile("-");
		Matcher match_00 = patt_00.matcher(word);
		if(match_00.find()){
			/* Splits word */
			String[] pieces = word.split("-");
			/* If word has two parts: guarda-chuva / splittedGrapheme = #guarda-chuva#*/
			if(pieces.length==2){
				splittedWord = new String[2];
				splittedWord[0] = pieces[0]; /* splittedGrapheme[0]    = ##guarda##*/ 
				splittedWord[1] = pieces[1];    /* splittedGrapheme[1] = ##chuva## */
			}
			/* If word has tree parts: chapeu-de-sol */
			if(pieces.length==3){
				splittedWord = new String[3];
				splittedWord[0] = pieces[0];   /* splittedGrapheme[0]    = ##chapeu##*/
				splittedWord[1] = pieces[1];  /* splittedGrapheme[1] = ##de## */
				splittedWord[2] = pieces[2];      /* splittedGrapheme[2] = ##sol## */
			}
			/* Exist? */
			if(pieces.length>3){
				System.err.println("BAD WORD FORMAT: "+ splittedWord[0]);
				return null;
			}
		}else {
			/* No hyphen word*/
			splittedWord[0]=word;
		}
		
		return splittedWord;
	}

	
}
