package br.ufpa.laps.jlapsapi.util.conversor;
//package components.stress;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StressRuleBased02 extends StressComponent{

	
	/* This words should obey rule gramm-17, but are exceptions.*/
	private static String[] rulegramm17_exceptions={"#feijoada#",
		"#caipirinha#","#caipirinhas#","#saudade#", "#saudades#","maudade"};
	

	
	/**
	 * Determines the stressed vowel from a string. This method is
	 * based in paper:
	 * 
	 * A. Siravenha, N. Neto, V. Macedo, and A. Klautau, 
	 * “Uso de Regras Fonologicas com Determinacao de Vogal Tonica 
	 * para Conversao Grafema-Fone em Portugues Brasileiro,” 7th 
	 * International Information and Telecommunication Technologies 
	 * Symposium, 2008
	 * 
	 * @param word the grapheme.
	 * @return the index of stressed letter. if 0, stress not found. 
	**/
	@Override
	public int process(String word) {

		/* Tests if Word has a grapheme */
		if (word.equals("")){
			System.err.println("No word to compute!" );
			return 0;
		}
			
		/* Get word grafeme adding auxiliar string (#) and verifies if has hyphen */
		String grapheme="#"+word+"#";
		/* String that will be process */
		String [] splittedGrapheme = new String[1];
		/* Rule of hyphen - Words with hyphen */
		Pattern patt_00 = Pattern.compile("-");
		Matcher match_00 = patt_00.matcher(grapheme);
		if(match_00.find()){
			/* Splits word */
			String[] pieces = grapheme.split("-");
			/* If word has two parts: guarda-chuva / splittedGrapheme = #guarda-chuva#*/
			if(pieces.length==2){
				splittedGrapheme = new String[2];
				splittedGrapheme[0]    = pieces[0]+"#"; /* splittedGrapheme[0]    = #guarda#*/ 
				splittedGrapheme[1] = "#"+pieces[1];    /* splittedGrapheme[1] = #chuva# */
			}
			/* If word has tree parts: chapeu-de-sol */
			if(pieces.length==3){
				splittedGrapheme = new String[3];
				splittedGrapheme[0]    = pieces[0]+"#";   /* splittedGrapheme[0]    = #chapeu#*/
				splittedGrapheme[1] = "#"+pieces[1]+"#";  /* splittedGrapheme[1] = #de# */
				splittedGrapheme[2] = "#"+pieces[2];      /* splittedGrapheme[2] = #sol# */
			}
			/* Exist? */
			if(pieces.length>3){
				System.err.println("BAD WORD FORMAT: "+ splittedGrapheme[0]);
				return 0;
			}
		}else {
			/* No hyphen word*/
			splittedGrapheme[0]=grapheme;
		}
		
		/* Counts additional indexes if word has hyphen */
		int addPosCount=0;
		for (int i = 0; i < splittedGrapheme.length-1; i++) 
			addPosCount+=splittedGrapheme[i].replaceAll("#", "").length()+1;
		
		int position=addPosCount;
		String letter;
		String subText;
		/* Search only in more right word*/	
		String text = splittedGrapheme[splittedGrapheme.length-1];

		if(word.equals("sorteio"))
			System.out.print("");
		
		/* Finally, the rules */
		/* precedence rules */
		/* Rule prec-01 - Is there accented vowel? acute and "crase" - facil */
		Pattern patt_01 = Pattern.compile("([áéíóúâêôàè])");
		Matcher match_01 = patt_01.matcher(text);
		if (match_01.find()) {
			subText=match_01.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_01.start());
			//word.setStress(letter, position, "rule01");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule01");
			return position;
		} 
		
		/* Rule prec-02 - Ends in r, l, z, x and n? fazer   */
		Pattern patt_02 = Pattern.compile("([^#][rlzxn][#])");
		Matcher match_02 = patt_02.matcher(text);
		if(match_02.find()){
			
			subText=match_02.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_02.start());
			/* Verify if is a vowel */
			if(isVowel(letter)){
				//word.setStress(letter, position, "rule02");
				//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule02");
				return position;
			}else{
				//System.out.println(text+"|"+"Stressed vowel not found. Possible foreign world.");
				return position;
			}			

		}	
		
		/* Rule prec-03 - tilde  - first an*/
		Pattern patt_03 = Pattern.compile("([ãõ])");
		Matcher match_03 = patt_03.matcher(text);
		if(match_03.find()){
			subText=match_03.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_03.start());
			//word.setStress(letter, position, "rule03");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule03");
			return position;
		}
		
		/* grammatic rules */
		/* Rule gramm-01 - V ou V-C */
		Pattern patt_04 = Pattern.compile("(([#][aeo][#])|([#][aeiou][^aeiou][#]))");
		Matcher match_04 = patt_04.matcher(text);
		if(match_04.find()){
			subText=match_04.group();
			letter=subText.substring(1, 2);
			position+= getWordIndex(subText, letter, match_04.start());
			//word.setStress(letter, position, "rule04");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule04");
			return position+1;
		}
		
		/* Rule gramm-02 - C-V ou C-V-C */
		Pattern patt_05 = Pattern.compile("(([#][^aeiou][aeiou][#])|([#][^aeiou][aeiou][^aeiou][#]))");
		Matcher match_05 = patt_05.matcher(text);
		if(match_05.find()){
			subText=match_05.group();
			letter=subText.substring(2, 3);
			position+= getWordIndex(subText, letter, match_05.start());
			//word.setStress(letter, position, "rule05");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule05");
			return position + 1;
		}
		/* Rule gramm-02 - C-C-V-C */
		Pattern patt_06 = Pattern.compile("([#][^aeiou][^aeiou][aeiou][^aeiou][#])");
		Matcher match_06 = patt_06.matcher(text);
		if(match_06.find()){
			subText=match_06.group();
			letter=subText.substring(3, 4);
			position+= getWordIndex(subText, letter, match_06.start());
			//word.setStress(letter, position, "rule06");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule06");
			return position;
		}
		
		/* Rule gramm-03 - i o u seguido de m final de frase */
		Pattern patt_07 = Pattern.compile("([iou]m#)");
		Matcher match_07 = patt_07.matcher(text);
		if(match_07.find()){
			subText=match_07.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_07.start());
			//word.setStress(letter, position, "rule07");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule07");
			return position;
		}
		
		/* Rule gramm-04 - i o u seguido de ns final de frase */
		Pattern patt_08 = Pattern.compile("([iou]ns#)");
		Matcher match_08 = patt_08.matcher(text);
		if(match_08.find()){
			subText=match_08.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_08.start());
			//word.setStress(letter, position, "rule08");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule08");
			return position;
		}
		
		/* Rule gramm-05 - aqui caqui */
		Pattern patt_09 = Pattern.compile("[qg][u][i]#");
		Matcher match_09 = patt_09.matcher(text);
		if(match_09.find()){
			subText=match_09.group();
			letter=subText.substring(2, 3);
			position+= getWordIndex(subText, letter, match_09.start());
			//word.setStress(letter, position, "rule09");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule09");
			return position;
		}
		
		/* Rule gramm-06 - caquis */
		Pattern patt_10 = Pattern.compile("[qg][u][i]s#");
		Matcher match_10 = patt_10.matcher(text);
		if(match_10.find()){
			subText=match_10.group();
			letter=subText.substring(2, 3);
			position+= getWordIndex(subText, letter, match_10.start());
			//word.setStress(letter, position, "rule10");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule10");
			return position;
		}
		
		/* Rule gramm-07 - cai ai ei estou */
		Pattern patt_11 = Pattern.compile("[aeiou][iu]#");
		Matcher match_11 = patt_11.matcher(text);
		if(match_11.find()){
			subText=match_11.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_11.start());
			//word.setStress(letter, position, "rule11");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule11");
			return position;
		}
		
		/* Rule gramm-08 - guarani itu */
		Pattern patt_12 = Pattern.compile("[^aeiou][iu]#");
		Matcher match_12 = patt_12.matcher(text);
		if(match_12.find()){
			subText=match_12.group();
			letter=subText.substring(1, 2);
			position+= getWordIndex(subText, letter, match_12.start());
			//word.setStress(letter, position, "rule12");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule12");
			return position;
		}
		
		/* Rule gramm-09 - cais vendavais ous */
		Pattern patt_13 = Pattern.compile("[aeiou][iu]s#");
		Matcher match_13 = patt_13.matcher(text);
		if(match_13.find()){
			subText=match_13.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_13.start());
			//word.setStress(letter, position, "rule13");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule13");
			return position;
		}
		
		/* Rule gramm-10 - guaranis tupis */
		Pattern patt_14 = Pattern.compile("[^aeiou][iu]s#");
		Matcher match_14 = patt_14.matcher(text);
		if(match_14.find()){
			subText=match_14.group();
			letter=subText.substring(1, 2);
			position+= getWordIndex(subText, letter, match_14.start());
			//word.setStress(letter, position, "rule14");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule14");
			return position;
		}
		
		/* Rule gramm-11 - porque */
		Pattern patt_15 = Pattern.compile("[p][o][r][q][u][e][#]");
		Matcher match_15 = patt_15.matcher(text);
		if(match_15.find()){
			subText=match_15.group();
			letter=subText.substring(5, 6);
			position+= getWordIndex(subText, letter, match_15.start());
			//word.setStress(letter, position, "rule15");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule15");
			return position;
		}
		
		/* Rule gramm-12 - almanaque caiaque toque */
		Pattern patt_16 = Pattern.compile("[aeiou][qg]ue#");
		Matcher match_16 = patt_16.matcher(text);
		if(match_16.find()){
			subText=match_16.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_16.start());
			//word.setStress(letter, position, "rule16");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule16");
			return position;
		}
		
		/* Rule gramm-13 - almanaques caiaques toques apliquem */
		Pattern patt_17 = Pattern.compile("[aeiou]que[sm]#");
		Matcher match_17 = patt_17.matcher(text);
		if(match_17.find()){
			subText=match_17.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_17.start());
			//word.setStress(letter, position, "rule17");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule17");
			return position;
		}
		
		/* Rule gramm-13 - maia gandaia meio  */
		Pattern patt_18 = Pattern.compile("[aeiou][iu][aeiou][s#]");
		Matcher match_18 = patt_18.matcher(text);
		if(match_18.find()){
			letter=match_18.group().replaceAll("#", "");
			position+=text.indexOf(letter);
			letter=letter.substring(0,1);
			//word.setStress(letter, position, "rule18");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule18");
			return position;
		}
			/* Rule gramm-14 - paira neuro pouco loira coisa doida  */
		Pattern patt_19 = Pattern.compile("([^qg][aeiou][iu][^aeiou][aeiou][#])");
		Matcher match_19 = patt_19.matcher(text);
		if(match_19.find()){
			subText=match_19.group();
			letter=subText.substring(1, 2);
			position+= getWordIndex(subText, letter, match_19.start());
			//word.setStress(letter, position, "rule19");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule19");
			return position;
			}
		
		/* Rule gramm-15 - ouros poucos loiras */
		Pattern patt_20 = Pattern.compile("([^qg][aeiou][iu][^aeiou][aeiou][s][#])");
		Matcher match_20 = patt_20.matcher(text);
		if(match_20.find()){
			subText=match_20.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_20.start());
			//word.setStress(letter, position, "rule20");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule20");
			return position;
		}
		
		/* Rule gramm-16 - ainda caindo fuinha */
		Pattern patt_21 = Pattern.compile("([aeiou][iu][n][^aeiou][aeo][#])");
		Matcher match_21 = patt_21.matcher(text);
		if(match_21.find()){
			subText=match_21.group();
			letter=subText.substring(1, 2);
			position+= getWordIndex(subText, letter, match_21.start());
			//word.setStress(letter, position, "rule21");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule21");
			return position;
		}
		
		/* Rule gramm-17 -  não tenho exemplos  bairros =[ */
		Pattern patt_22 = Pattern.compile("[^qg#][aeiou][iu][^aeiou]");
		Matcher match_22 = patt_22.matcher(text);
		if(match_22.find() && !hasString(rulegramm17_exceptions, text)){
			subText=match_22.group();
			letter=subText.substring(1, 2);
			position+= getWordIndex(subText, letter, match_22.start());
			//word.setStress(letter, position, "rule22");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule22");
			return position;
		}
			/* Rule gramm-18 -  quem */
		Pattern patt_23 = Pattern.compile("[q][u][e][m][#]");
		Matcher match_23 = patt_23.matcher(text);
		if(match_23.find()){
			subText=match_23.group();
			letter=subText.substring(2, 3);
			position+= getWordIndex(subText, letter, match_23.start());
			//word.setStress(letter, position, "rule23");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule23");
			return position;
		}
		
		/* Rule gramm-19 -  que  */
		Pattern patt_24 = Pattern.compile("[q][u][e][#]");
		Matcher match_24 = patt_24.matcher(text);
		if(match_24.find()){
			subText=match_24.group();
			letter=subText.substring(2, 3);
			position+= getWordIndex(subText, letter, match_24.start());
			//word.setStress(letter, position, "rule24");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule24");
			return position;
		}
		/* If word didnt obey any previous rule. In paper, it is rule 20 */
		//if(find==false){
		/* Rule gramm-20 - V-V no fim de frase */
		Pattern patt_25 = Pattern.compile("([aeiou][aeiou])#");
		Matcher match_25 = patt_25.matcher(text);
		if(match_25.find()){
			subText=match_25.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_25.start());
			//word.setStress(letter, position, "rule25");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule25");
			return position;
		}
		
		/* Rule gramm-21 - V-C-V or V-V-C no fim de frase casa tabaco aba ata moer*/
		Pattern patt_26 = Pattern.compile("(([aeiou][^aeiou][aeiou])|([aeiou][aeiou][^aeiou]))#");
		Matcher match_26 = patt_26.matcher(text);
		if(match_26.find()){
			subText=match_26.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_26.start());
			//word.setStress(letter, position, "rule26");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule26");
			return position;
		}
		
		/* Rule gramm-22 -  V-C-C-V or V-C-V-C  alho acre palha empreendedor ator */
		Pattern patt_27 = Pattern.compile("(([aeiou][^aeiou][^aeiou][aeiou])|" +
				"([aeiou][^aeiou][aeiou][^aeiou]))#");
		Matcher match_27 = patt_27.matcher(text);
		if(match_27.find()){
			subText=match_27.group();
			letter=subText.substring(0, 1);
			position+=getWordIndex(subText, letter, match_27.start());
			//word.setStress(letter, position, "rule27");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule27");
			return position;
		}
		/* Rule gramm-23 -  V-C-C-V-C or V-C-C-C-V or V-C-V-C-C */
		Pattern patt_28 = Pattern.compile("(([aeiou][^aeiou][^aeiou][aeiou][^aeiou])|" +
				"([aeiou][^aeiou][^aeiou][^aeiou][aeiou])|" +
				"([aeiou][^aeiou][aeiou][^aeiou][^aeiou]))#");
		Matcher match_28 = patt_28.matcher(text);
		if(match_28.find()){
			subText=match_28.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_28.start());
			//word.setStress(letter, position, "rule02");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule28");
			return position;
		}
			
		/* Rule gramm-24 - V-C-C-C-V-C  */      
		Pattern patt_29 = Pattern.compile("(([aeiou][^aeiou][^aeiou][^aeiou][aeiou][^aeiou]))#");
		Matcher match_29 = patt_29.matcher(text);
		if(match_29.find()){
			subText=match_29.group();
			letter=subText.substring(0, 1);
			position+= getWordIndex(subText, letter, match_29.start());
			//word.setStress(letter, position, "rule02");
			//System.out.println(text.replaceAll("#", "")+"|"+letter+"|"+position+"|"+"rule29");
			return position;
			
		}
		
		/* Reached the end and not found =[ */
		//System.err.println("Stress not found, verify: "+ text);
		return 0;
	}



	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	
//	/* Only for tests */
//	public static void main(String[] args) {
//		
//		try {
//			/* Open the a object */
//			File inputFile = new File("/home/04080004701/workspace/TextAnalalysisTools4PB/tmp"); 
//			/* Create a buffer to read words */
//			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
//			/* Verifies if the current line has data: yes - stores in String Array / no - reading finished */
//			String line;
//			while ( (line = reader.readLine())!=null ) 
//			       /* Stores in LinkedList */
//				System.out.println(line +" "+compute_Algorithm01(line));
//			reader.close();
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
//		
//	}
}
