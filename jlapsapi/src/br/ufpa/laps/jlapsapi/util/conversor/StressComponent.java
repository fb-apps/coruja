package br.ufpa.laps.jlapsapi.util.conversor;
/**
 * This is an abstract indicator of stress.
 * @autor Igor Couto
**/

//package components.stress;

public abstract class StressComponent {

	/**
	 * Determines the stressed vowel from a string. 
	 * @param input a word.
	 * @return the index of stressed vowel.
	 * */
	public abstract int process(String input);
	
	/**
	 * Returns the name of G2PComponent
	**/
	public abstract String getName();
	
	
	/**
	 * This method only returns true if letter is a vowel.
	 * @param letter the letter that will be analyzed.  
	 * @return true if letter is a vowel
	 * */
	public static boolean isVowel(String letter){
		/* Set of vowels in brazilian portuguese */
		String [] vowels ={"a","e","i","o","u",
				"á","é","í","ó","ú","à","è","ã",
				"õ"};
		/* Search letter inside vowels */
		for(String temp:vowels){
			if (letter.equals(temp))
				return true;
		}
		return false;
	}
	
	/**
	 * Java has a different way to handle regular expressions.
	 * This class is necessary to determine the exact index of 
	 * a word. Maybe, all rules should use it.
	 * */
	public static int getWordIndex(String subText, String letter, int nPreviousLetters){
		/* Remove auxiliary characters. */
		subText = subText.replace("#", "");
		/* Get index in subText and increase index value with nPreviousLetters. ab[a]ixo, not [a]baixo
		 * text = #abaixo# 
		 *  subText =  baixo#
		 *  letter = a
		 *  nPreviousLetters = 2
		 *  least one because of the first # 
		 */
		int index = subText.indexOf(letter);
		int realIndex = index + nPreviousLetters;
		
		return realIndex;
	}
	
	/** 
	 * Only verifies if a array contains a specific string.  
	 * @param array Array with strings
	 * @param text string to search
	 * @return true if array contains text
	 */
	public static boolean hasString(String[] array, String text){
		/* Search string inside array */
		for(String temp:array){
			if(text.equals(temp))
				return true;
		}
		return false;
	}
}
