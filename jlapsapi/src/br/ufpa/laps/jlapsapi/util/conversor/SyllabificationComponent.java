package br.ufpa.laps.jlapsapi.util.conversor;
/**
 * An abstract separator of syllables.
 * <p>
 * 
 * Last modification in May 29, 2010.
 * @author Igor Couto
 * 
 * */

//package components.syll;

//import components.Component;

public abstract class SyllabificationComponent implements Component {
	
	/**
	 * Computes the syllabification. 
	 * @param word the word
	 * @return the separate word
	 * */
	
	public abstract String process(String word);
	

	
	
}
