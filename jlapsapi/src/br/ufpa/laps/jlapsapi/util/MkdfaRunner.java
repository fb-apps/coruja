package br.ufpa.laps.jlapsapi.util;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;


public class MkdfaRunner {
	//Execução do compilador de gramática
	
	public static void execMkdfapl(String grammar){
		final URI uri;
		final String appPath;
		
		try {
			uri = ZipExtractor.getJarURI();
			
			appPath = uri.toString().substring(5, uri.toString().substring(5).length()-12);
			
			int x = Runtime.getRuntime().exec(appPath+"/gramtools/mkdfa.pl "+grammar).waitFor();
			if(x!=0) System.out.println("Falha na compilação da gramática");

		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block

			//PARA EXECUÇÃO NO ECLIPSE
			int x;
			try {
				x = Runtime.getRuntime().exec("./gramtools/mkdfa.pl "+grammar).waitFor();
				if(x!=0) System.out.println("Falha na compilação da gramática");
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}		
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}
}