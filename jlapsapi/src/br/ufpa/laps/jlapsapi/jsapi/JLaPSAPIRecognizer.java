package br.ufpa.laps.jlapsapi.jsapi;

import java.io.IOException;
import java.io.Reader;

import javax.speech.EngineException;
import javax.speech.EngineStateError;
import javax.speech.recognition.DictationGrammar;
import javax.speech.recognition.GrammarException;
//import javax.speech.recognition.Rule;
import javax.speech.recognition.RuleGrammar;

import br.ufpa.laps.jlapsapi.recognizer.Recognizer;

import com.sun.speech.engine.recognition.BaseDictationGrammar;
import com.sun.speech.engine.recognition.BaseRecognizer;
import com.sun.speech.engine.recognition.BaseRuleGrammar;

public class JLaPSAPIRecognizer extends BaseRecognizer {

	private JSGFGrammar grammar;

	private Recognizer recognizer;

	JLaPSAPIResultListener resultListener;

	RecognitionThread recognitionThread;

	public JLaPSAPIRecognizer() throws EngineException {
		this(new JLaPSAPIRecognizerModeDesc());
	}

	public JLaPSAPIRecognizer(JLaPSAPIRecognizerModeDesc modeDesc) {
		recognizer = new Recognizer();
	}

	protected void handleAllocate() {

		try{
			recognizer.allocate();
		} catch(EngineException exception){
			System.err.println(exception.getMessage());
			return;
		}

		resultListener = new JLaPSAPIResultListener(this);
		recognizer.addResultListener(resultListener);
		setEngineState(CLEAR_ALL_STATE, ALLOCATED);
	}

	protected void handleResume() {
		recognizer.resume();
		if (recognitionThread == null) {
			recognitionThread = new RecognitionThread();
			recognitionThread.start();
		}
		setEngineState(CLEAR_ALL_STATE, RESUMED);
	}

	protected void handlePause() {
		recognizer.pause();
		if (recognitionThread != null) {
			recognitionThread.stopRecognition();
		}
		setEngineState(CLEAR_ALL_STATE, PAUSED);
	}

	protected void handleDeallocate() throws EngineException {
		recognizer.deallocate();
		if (recognitionThread != null) {
			recognitionThread.stopRecognition();
		}
	}

	public RuleGrammar loadJSGF(Reader reader) throws GrammarException,
				IOException, EngineStateError {
		//Sets the ruleGrammar Name
		String grammarName = recognizer.loadJSGF(reader);
		RuleGrammar ruleGrammar = new BaseRuleGrammar(this, grammarName);

		/*
		final RuleGrammar loadedGrammar = super.loadJSGF(reader);
		final RuleGrammar ruleGrammar = grammar.getRuleGrammar();
		final String[] loadedRuleNames = loadedGrammar.listRuleNames();
		for (String name : loadedRuleNames) {
			final Rule rule = loadedGrammar.getRule(name);
			ruleGrammar.setRule(name, rule, true);
			ruleGrammar.setEnabled(name, true);
		}*/
		//grammar.commitChanges();
		//return loadedGrammar;
		return ruleGrammar;
	}

	public DictationGrammar getDictationGrammar(String name){
		//Sets the dictationGrammar Name
		DictationGrammar dictationGrammar = new BaseDictationGrammar(this, name);
		return dictationGrammar;
	}

	RuleGrammar getRuleGrammar() {
		return grammar.getRuleGrammar();
	}



}
