/*package br.ufpa.laps.jlapsapi.jsapi;

public class RecognitionThread extends Thread{
	
	public void run(){
		startRecognition();
	}
	
	private void startRecognition(){
		while(!isInterrupted()){
			
		}
	}
	
	public void stopRecognition(){
		interrupt();
	}

}
*/

package br.ufpa.laps.jlapsapi.jsapi;

public class RecognitionThread extends Thread{
	boolean pleasewait = false;//novo
 
	public void run(){
		startRecognition();

		while(true){//novo
			synchronized(this){//novo
				while(pleasewait){//novo
					try{//novo
						wait();//novo
					}catch (Exception e){//novo
						e.printStackTrace();
					}
				}
			}
		} 
	}
 
	private void startRecognition(){
		/*
 		while(!isInterrupted()){
 		}*/
		synchronized(this){//novo
			pleasewait=false;//novo
			this.notify();//novo
		}
	}

	public void stopRecognition(){
		synchronized(this){//novo
			pleasewait=true;//novo
		}
		//interrupt();
	}
}

