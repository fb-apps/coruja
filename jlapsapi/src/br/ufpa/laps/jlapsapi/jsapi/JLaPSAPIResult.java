package br.ufpa.laps.jlapsapi.jsapi;

import javax.speech.recognition.Grammar;

import br.ufpa.laps.jlapsapi.recognizer.Result;

import com.sun.speech.engine.recognition.BaseResult;

public class JLaPSAPIResult extends BaseResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JLaPSAPIResult(final Grammar grammar, final Result result) {
		theText = formatUtterance(result.getUtterance());
		setnTokens(theText.length);
	}

	private String[] formatUtterance(String utterance) {
		
		while(utterance.charAt(0) == ' '){
			utterance = utterance.substring(1);
		}
		
		return utterance.split(" ");
	}

}
