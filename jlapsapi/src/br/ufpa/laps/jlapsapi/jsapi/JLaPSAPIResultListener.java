package br.ufpa.laps.jlapsapi.jsapi;

import javax.speech.recognition.ResultEvent;

import br.ufpa.laps.jlapsapi.recognizer.Result;


public class JLaPSAPIResultListener implements ResultListener{
	
	private JLaPSAPIRecognizer recognizer;
	
	public JLaPSAPIResultListener(JLaPSAPIRecognizer recognizer) {
		this.recognizer = recognizer;
	}

	@Override
	public void resultAccepted(Result result) {
		JLaPSAPIResult jResult = new JLaPSAPIResult(null, result);
		ResultEvent resultEvent = new ResultEvent(jResult, ResultEvent.RESULT_ACCEPTED);
		recognizer.fireResultAccepted(resultEvent);	
	}
	
	

}
