package br.ufpa.laps.jlapsapi.recognizer;

import javax.speech.Central;
import javax.speech.recognition.Recognizer;
import javax.speech.recognition.RecognizerModeDesc;
import javax.speech.recognition.Result;
import javax.speech.recognition.ResultAdapter;
import javax.speech.recognition.ResultEvent;
import javax.speech.recognition.ResultToken;
import javax.speech.recognition.RuleGrammar;
import javax.speech.recognition.DictationGrammar;

//import java.io.FileReader;
//import java.util.Scanner;

//mexendo recognizer, configurationManager, Recognizer.cpp, SREngine.cpp

public class SimpleRecognition extends ResultAdapter {

	static Recognizer rec;
	static RuleGrammar gram;
	static DictationGrammar dic;

	public void resultAccepted(ResultEvent e) {
		//rec.pause();
		try {
			//int n;
			Result r = (Result) (e.getSource());
			ResultToken tokens[] = r.getBestTokens();
			for (int i = 0; i < tokens.length; i++)
				System.out.print(tokens[i].getSpokenText()+" ");
			
			
			//System.out.println("\nDigite 0 para sair");
//			Scanner x = new Scanner(System.in);
			
/*			if(x.nextInt()==0){
				rec.deallocate();
				System.exit(0);
*/			//} else {

/*				if(dic.isEnabled()){
					//dic.setEnabled(false);
					//gram.setEnabled(true);
					System.out.println("\nEnableGrammar");
				} else {
					//gram.setEnabled(false);
					//dic.setEnabled(true);
					System.out.println("\nEnableDictationGrammar");
				}*/
				//rec.resume();
				System.out.println("\n<<<<<<<Recognizing again>>>>>>>>>>");
			//}
		}catch (Exception e1) {
			e1.printStackTrace(); 
		}
	}

	/**
	 * @param args
	 */
	public static void main(String args[]) {


		try {

			RecognizerModeDesc rmd = (RecognizerModeDesc) Central
			.availableRecognizers(null).firstElement();
			
			rec = Central.createRecognizer(rmd);
			System.out.println("rec created");
			
			rec.allocate();		
			System.out.println("rec allocated");
			

			//COLOQUE UMA GRAMÁTICA EM UM DIRETÓRIO VÁLIDO NA SUA MÁQUINA PARA TESTAR A GRAMÁTICA
			//FileReader reader = new FileReader("/home/10080000701/Documents/drinks.grammar");			
			//System.out.println("load gram");
			//gram = rec.loadJSGF(reader);
			
			System.out.println("load dic");
			dic = rec.getDictationGrammar("dicSr");			
			
			//dic.setEnabled(false);
			//gram.setEnabled(false);
			
			//NÃO FUNCIONA AINDA
			//dic.addResultListener(new ListenerRec1());
			//gram.addResultListener(new ListenerRec2());
			
			rec.addResultListener(new SimpleRecognition());
			
			rec.resume();
			System.out.println("rec resume");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
