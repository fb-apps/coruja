
#include "stdafx.h"

#include "RecoResult.h"

namespace LapsAPI{

	RecoResult::RecoResult() {
	}

	RecoResult::RecoResult(String^ uterrance, float confidence) {
		this->uterrance = uterrance;
		this->confidence = confidence;
	}

	String^ RecoResult::getUterrance(){
		return this->uterrance;
	}

	float RecoResult::getConfidence(){
		return this->confidence;
	}
}