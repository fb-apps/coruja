#include "stdafx.h"

#include "Util.h"
#include <cstdlib>

using System::String;
using System::Char;
using System::Int32;

namespace MyUtils{

	char* Util::string2Char(String ^s){

		int len = Int32::Parse(s->Length.ToString());
		char* cString = (char*) malloc(sizeof(char) * (len + 1));
		
		for (int i = 0; i < len; i++){
			cString[i] = s[i];
		}
		cString[len] = '\0';
		return cString;
	}

}