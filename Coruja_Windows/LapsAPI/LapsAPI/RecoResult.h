// RecogResult.h

#pragma once

using System::String;

namespace LapsAPI {

	public ref class RecoResult
	{
	public:
		RecoResult();
		RecoResult(String^ uterrance, float confidence);
		String^ getUterrance();
		float getConfidence();
	private:
		String^ uterrance;
		float confidence;
	};
}
