/**********************************************************
 * SREngine.h
 *
 *  Created on: Aug 26, 2009 8:28:33 AM
 *      Author: Pedro Batista
 *      Federal University of Par�
 +********************************************************/

#ifndef SRENGINE_H_
#define SRENGINE_H_

#pragma once

#include "RecoResult.h"

extern "C"{
#include <julius/julius.h>
}

namespace LapsAPI {
	
	/**
	* Class to implement julius (decoder) and recognizer streams
	*/
	public ref class SREngine
	{
	public:
		/**
		* Delegate to be called when somethink is recognized
		*/
		delegate void OnRecognitionDele(LapsAPI::RecoResult^ result);
		static property OnRecognitionDele^ OnRecognition;

		/**
		* Delegate to be called when recognicion is active and running
		* i.e. when startRecognition is called
		*/
		delegate void OnSpeechReadyDele();
		property OnSpeechReadyDele^ OnSpeechReady;
		/**
		* Just allocate memory for recognizer
		*/
		SREngine();
		/**
		* Creante an instance of the recognizer with configurations
		* give in the jconfFilePath (jconf file)
		*/
		SREngine(String^ jconfPath);
		/**
		* Destructor just free memory
		*/
		~SREngine(); 
		/**
		* Assumes that the input is from mic and when something is
		* recognizer execute the methods added with OnRecognition
		*/
		void startRecognition();
		void dSREngine();
		/**
		* Stop recognition (stop recognition stream)
		*/
		void stopRecognition();
	private:
		Recog *recog;
		Jconf *jconf;
	};
}

#endif /* SRENGINE_H_ */