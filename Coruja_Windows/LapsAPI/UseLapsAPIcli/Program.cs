﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LapsAPI;

namespace UseLapsAPIcli
{
    class Program
    {
        private Recognizer recog;

        public Program()
        {
            recog = new Recognizer("G:\\AM\\ComandAndControl\\julius.CmdConf");
            Recognizer.RecognizedAction += acao;
            recog.SpeechReady += ready;
        }

        void acao(RecogResult result)
        {
            Console.WriteLine("Sentença: " + result.getUterrance() + " Confiança: " + result.getConfidence());
            Console.WriteLine("Continuar? y/n");
            string re = Console.In.ReadLine();
            if (re.Equals("n"))
                recog.stopRegognition();
        }

        void ready()
        {
            Console.WriteLine("----Escutando feliz-----");
        }

        void run()
        {
            string re;
            do
            {
                recog.startRecognition();
                Console.WriteLine("Começar? y/n");
                re = Console.In.ReadLine();
            } while (re.Equals("y"));
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.run();
            /*
            Recognizer re = new Recognizer("G:\\AM\\julius.jconf");
            Recognizer.RecognizedAction += Program.acao;
            re.SpeechReady += Program.ready;
            Console.WriteLine("as");
            re.startRecognition();
            RecogResult result = new RecogResult("Pedro", 1);
            Console.WriteLine("Sentença: " + result.getUterrance() + " Confiança: " + result.getConfidence());
            String a = "pedro";
            Console.WriteLine(a.Length.ToString());
            //int a = Int32.Parse();
            Console.WriteLine(a.Length.ToString());*/
        }
    }
}
