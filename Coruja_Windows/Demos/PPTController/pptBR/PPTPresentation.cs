﻿

namespace pptBR
{
    using System;
    //using Power = Microsoft.Office.Interop.PowerPoint;
    using Power = Microsoft.Office.Interop.PowerPoint;

    class PPTPresentation
    {

        private Power.Application app = null;

        private Power.Presentation ppt = null;

        private Power.SlideShowWindow window = null;

        private String fileName = null;

        /// <summary>
        /// Construtor que recebe o endereço da apresentação
        /// </summary>
        /// <param name="fileName"></param>Endereço do arquivo
        public PPTPresentation(String fileName)
        {
            this.fileName = fileName;
        }

        /// <summary>
        /// Abre a apresentação que está em fileName
        /// </summary>
        public void openPPT()
        {
            //Inicializa a aplicação
            app = new Microsoft.Office.Interop.PowerPoint.Application();

            app.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;
            //Abre a apresentação
            ppt = app.Presentations.Open(fileName,
                                        Microsoft.Office.Core.MsoTriState.msoCTrue,
                                        Microsoft.Office.Core.MsoTriState.msoFalse,
                                        Microsoft.Office.Core.MsoTriState.msoTrue);
        }

        public void Close()
        {
            ppt.Close();
        }

        public void showPresentation()
        {
            window = ppt.SlideShowSettings.Run();
        }

        public void exitPresentation()
        {
            window.View.Exit();
        }

        public void next()
        {
            window.View.Next();
        }
        public void previous()
        {
            window.View.Previous();
        }

        public void gotoSlide(int slide)
        {
            window.View.GotoSlide(slide, Microsoft.Office.Core.MsoTriState.msoFalse);
        }

        public Power.SlideShowView getView()
        {
            return window.View;
        }

    }
}
