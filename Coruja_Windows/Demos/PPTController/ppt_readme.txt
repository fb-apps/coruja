####

O pacote de distribui��o da LapsAPI possui tr�s dlls: julius.dll, sent.dll e LapsAPI.dll. As duas primeiras possuem c�digos do reconhecedor Julius e devem ser copiadas para a pasta do sistema operacional: C:\WINDOWS\system. J� a �ltima biblioteca � a LapsAPI, propriamente dita, que deve ser inclu�da como refer�ncia no seu c�digo fonte.

####

Um erro comum de compila��o do PPTController � o endere�amento errado das refer�ncias, que podem ser visualizadas na aba "References" do Solution Explorer.
A dlls abaixo precisam estar adicionadas:
* LapsAPI: pode ser encontrada na pasta ..\LapsAPI\DLLs presente no pacote da API.
* Microsoft.Office.Core e Microsoft.Office.Interop.PowerPoint: para adicion�-las siga os passos abaixo:
- Click com o bot�o direito na aba "References" do Solution Explorer;
- Seleciona a op��o Add References;
- Na janela que se segue, dentro do aba COM, procure e selecione o objeto "Microsoft PowerPoint 12.0 Object Library";
- Em seguida, as duas dlls devem aparecer na aba "References";

####

O arquivo de configura��o para executar o PPTController pode ser encontrado na pasta ..\LapsAPI\jConfs\pptConf presente no pacote da API.
Caso ocorra algum erro durante o carregamento das configura��es, verifique o endere�amento dos modelos ac�sticos e de linguagem dentro do arquivo de configura��o, pois dependendo do diret�rio que o sistema tome como base para a aplica��o, ser� preciso alterar o caminho dos arquivos abaixo:

################### Caminho para a gram�tica #######################
-dfa "C:/Documents and Settings/Laps/My Documents/Coruja1.0/LapsAPI/Gramaticas/pptGrammar/comandos.dfa"
-v "C:/Documents and Settings/Laps/My Documents/Coruja1.0/LapsAPI/Gramaticas/pptGrammar/comandos.dict"

################### Hmm model #####################
-h "C:/Documents and Settings/Laps/My Documents/Coruja1.0/LapsAPI/LapsAM1.3/LapsAM1.3.bin"
-hlist "C:/Documents and Settings/Laps/My Documents/Coruja1.0/LapsAPI/LapsAM1.3/tiedlist"
-htkconf "C:/Documents and Settings/Laps/My Documents/Coruja1.0/LapsAPI/LapsAM1.3/edaz.conf"

O arquivo ..\LapsAPI\jConfs\pptConf\JuliusLog pode ajudar a encontrar a origem do erro.

####